﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(Resource))]
public class ResourceEditor : Editor {

    public override void OnInspectorGUI() {
        Resource resource = target as Resource;

        resource.resource = (SpawnableResources) EditorGUILayout.EnumPopup("Resource", resource.resource);
        resource.resourceType = (ResourceTypes) EditorGUILayout.EnumPopup("Type", resource.resourceType);
        resource.sprite = (Sprite) EditorGUILayout.ObjectField("Sprite", resource.sprite, typeof(Sprite), true);
        resource.model = (GameObject)EditorGUILayout.ObjectField("Model", resource.model, typeof(GameObject), true);
        resource.spawnBlock = (BoxCollider)EditorGUILayout.ObjectField("Spawn Block", resource.spawnBlock, typeof(BoxCollider), true);

        resource.spawnLimit = EditorGUILayout.IntField("Spawn Limit", resource.spawnLimit);
        resource.minLifetime = EditorGUILayout.FloatField("Min Lifetime", resource.minLifetime);
        resource.maxLifetime = EditorGUILayout.FloatField("Max Lifetime", resource.maxLifetime);
        resource.spawnChance = EditorGUILayout.FloatField("Spawn Chance", resource.spawnChance);

        resource.useAirPressure = GUILayout.Toggle(resource.useAirPressure, "Use Air Pressure");

        if (resource.useAirPressure) {
            resource.minAirPressure = EditorGUILayout.FloatField("Min Air Pressure", resource.minAirPressure);
            resource.maxAirPressure = EditorGUILayout.FloatField("Max Air Pressure", resource.maxAirPressure);
        }

        resource.useElectromagnetism = GUILayout.Toggle(resource.useElectromagnetism, "Use Electromagnetism");

        if (resource.useElectromagnetism) {
            resource.minElectromagnetism = EditorGUILayout.FloatField("Min Electromagnetism", resource.minElectromagnetism);
            resource.maxElectromagnetism = EditorGUILayout.FloatField("Max Electromagnetism", resource.maxElectromagnetism);
        }

        resource.useHumidity = GUILayout.Toggle(resource.useHumidity, "Use Humidity");

        if (resource.useHumidity) {
            resource.minHumidity = EditorGUILayout.FloatField("Min Humidity", resource.minHumidity);
            resource.maxHumidity = EditorGUILayout.FloatField("Max Humidity", resource.maxHumidity);
        }

        resource.usePrecipitation = GUILayout.Toggle(resource.usePrecipitation, "Use Precipitation");

        if (resource.usePrecipitation) {
            resource.minPrecipitation = EditorGUILayout.FloatField("Min Precipitation", resource.minPrecipitation);
            resource.maxPrecipitation = EditorGUILayout.FloatField("Max Precipitation", resource.maxPrecipitation);
        }

        resource.useSunExposure = GUILayout.Toggle(resource.useSunExposure, "Use Sun Exposure");

        if (resource.useSunExposure) {
            resource.minSunExposure = EditorGUILayout.FloatField("Min Sun Exposure", resource.minSunExposure);
            resource.maxSunExposure = EditorGUILayout.FloatField("Max Sun Exposure", resource.maxSunExposure);
        }

        resource.useTemperature = GUILayout.Toggle(resource.useTemperature, "Use Temperature");

        if (resource.useTemperature) {
            resource.minTemperature = EditorGUILayout.FloatField("Min Temperature", resource.minTemperature);
            resource.maxTemperature = EditorGUILayout.FloatField("Max Temperature", resource.maxTemperature);
        }

        resource.useWindSpeed = GUILayout.Toggle(resource.useWindSpeed, "Use Wind Speed");

        if (resource.useWindSpeed) {
            resource.minWindSpeed = EditorGUILayout.FloatField("Min Wind Speed", resource.minWindSpeed);
            resource.maxWindSpeed = EditorGUILayout.FloatField("Max Wind Speed", resource.maxWindSpeed);
        }

        EditorUtility.SetDirty(resource);
    }
}
