﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SpawnedCamera : MonoBehaviour {

    [HideInInspector]
    public bool isMovingCamera = false;
    [HideInInspector]
    public bool isZoomingCamera = true;
    [HideInInspector]
    public Transform target;

    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = 20f;
    public float yMaxLimit = 60f;

    public float distanceMin = 2f;
    public float distanceMax = 7f;

    private float x = 0.0f;
    private float y = 0.0f;


    // Use this for initialization
    void Start() {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        UpdateCameraPosition();
        UpdateCameraZoom();
    }

    void LateUpdate() {
        if (isMovingCamera) {
            UpdateCameraPosition();
        }

        if (isZoomingCamera) {
            UpdateCameraZoom();
        }

        UpdatePlayerRotation();
        UpdatePosition();
    }

    void UpdateCameraPosition() {
        if (target) {
            x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
            y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            transform.rotation = rotation;
        }
    }

    void UpdateCameraZoom() {
        if (target) {
            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
        }
    }

    void UpdatePosition() {
        Quaternion rotation = Quaternion.Euler(y, x, 0);
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target.position;

        transform.position = position;
    }

    void UpdatePlayerRotation() {
        //target.rotation = transform.rotation;
        //target.rotation = Quaternion.Euler(0, target.rotation.eulerAngles.y, 0);
    }

    public static float ClampAngle(float angle, float min, float max) {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
