﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Game : MonoBehaviour {

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

	public void StartGame() {
        SceneManager.LoadScene("Game");
    }

    public void Instructions() {
        SceneManager.LoadScene("Instructions");
    }

    public void Credits() {
        SceneManager.LoadScene("Credits");
    }

    public void Menu() {
        SceneManager.LoadScene("Menu");
    }
}
