﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Threading;

public struct IntVector2 : IEquatable<object> {
    public int x, y;


    public IntVector2(int[] xy) {
        x = xy[0];
        y = xy[1];
   } 

    public override bool Equals(object obj) {
        if (!(obj is IntVector2))
            return false;

        IntVector2 mys = (IntVector2)obj;

        return x == mys.x && y == mys.y;
    }

    public override int GetHashCode() {
        return base.GetHashCode();
    }
}

public static class ThreadSafeRandom {
    [ThreadStatic]
    private static System.Random Local;

    public static System.Random ThisThreadsRandom {
        get { return Local ?? (Local = new System.Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
    }
}

static class MyExtensions {
    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static IntVector2 ToIntVector2(this Vector2 vector2) {
        int[] intVector2 = new int[2];
        for (int i = 0; i < 2; ++i) intVector2[i] = Mathf.RoundToInt(vector2[i]);
        return new IntVector2(intVector2);
    }
}

public static class GameObjectExtensions {
    public static void ChangeLayers(this GameObject go, int layer) {
        go.layer = layer;
        foreach (Transform child in go.transform) {
            ChangeLayers(child.gameObject, layer);
        }
    }
}
