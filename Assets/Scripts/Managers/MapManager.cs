﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
    Creates maps for each attribute based on weather node positions.
    Computationally expensive.
    Exposes some static methods that handle converting between world coordinates and pixel coordinates
*/

public class MapManager : MonoBehaviour {

    public bool shouldCalculate;

    public int segmentRange;
    public float calculationCooldown = 2; // In game seconds

    public static int TextureHeight;
    public static int TextureWidth;
    public static float AspectRatio;

    public static float PixelToWorldXRatio;
    public static float PixelToWorldYRatio;

    public float pixelsPerUnit;

    public Color blankColor;

    private float _lastCalculationEndTime = -100;

    private bool _currentlyDrawing = false;

    // Normalize distributions so that they reach y~=0 at x=1 and y=1 at x=0
    private Dictionary<int, float[]> _memoizedValues = new Dictionary<int, float[]>();
    // Each index indicates the concentration
    private float[] _deltas = new float[]     {4,    3,      2,    1,    0.5f };
    private float[] _normalizeY = new float[] {0.1f, 0.135f, 0.2f, 0.4f, 0.8f };
    private float[] _xDistanceToYZero = new float[] {10,   8,      6,    3,    1.5f };

    private bool _calculating1 = true;
    private Dictionary<EnvironmentAttribute, float[]> _strengths1 = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, float[]> _strengths2 = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, float[]> _rawStrengths = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, float[]> _maxStrength = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, float[]> _maxNodeStrength = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, float[]> _nodeCounts = new Dictionary<EnvironmentAttribute, float[]>();
    private Dictionary<EnvironmentAttribute, bool[]> _nodeLocations1 = new Dictionary<EnvironmentAttribute, bool[]>();
    private Dictionary<EnvironmentAttribute, bool[]> _nodeLocations2 = new Dictionary<EnvironmentAttribute, bool[]>();

    void Awake() {
        TextureHeight = (int)(WeatherManager.BoundsY * pixelsPerUnit);
        TextureWidth = (int)(WeatherManager.BoundsX * pixelsPerUnit);
        AspectRatio = (float)TextureWidth / TextureHeight;

        PixelToWorldXRatio = WeatherManager.BoundsX / TextureWidth;
        PixelToWorldYRatio = WeatherManager.BoundsY / TextureHeight;

        for (int i = 0; i < _deltas.Length; i++) {
            _memoizedValues.Add(i, new float[101]);

            for (int j = 0; j < 101; j++) {
                _memoizedValues[i][j] = CalculateNormalizedDistributionValue(_deltas[i], (j / 100.0f) * _xDistanceToYZero[i], _normalizeY[i]);
            }
        }

        foreach (EnvironmentAttribute attr in System.Enum.GetValues(typeof(EnvironmentAttribute))) {
            _strengths1.Add(attr, new float[TextureWidth * TextureHeight]);
            _strengths2.Add(attr, new float[TextureWidth * TextureHeight]);
            _rawStrengths.Add(attr, new float[TextureWidth * TextureHeight]);
            _maxStrength.Add(attr, new float[TextureWidth * TextureHeight]);
            _maxNodeStrength.Add(attr, new float[TextureWidth * TextureHeight]);
            _nodeCounts.Add(attr, new float[TextureWidth * TextureHeight]);
            _nodeLocations1.Add(attr, new bool[TextureWidth * TextureHeight]);
            _nodeLocations2.Add(attr, new bool[TextureWidth * TextureHeight]);
        }
    }

    public void Tick(bool immediate = false) {
        //Debug.Log("Map Manager tick");
        if (shouldCalculate) {
            if (!_currentlyDrawing) {
                if (immediate) {
                    //Debug.LogFormat("Trying to calculate strengths {0}", GameTimeManager.GameTime);
                    IEnumerator e = CalculateStrengths(immediate);
                    e.MoveNext();
                } else if (GameTimeManager.GameTime - _lastCalculationEndTime > calculationCooldown) {
                    StartCoroutine(CalculateStrengths(immediate));
                }
            }
        }
    }

    // Use two textures that are switched and split across multiple frames
    IEnumerator CalculateStrengths(bool immediate) {
        //Debug.LogFormat("Calculating strengths {0}", GameTimeManager.GameTime);
        Dictionary<EnvironmentAttribute, float[]> writingToStrengths;
        Dictionary<EnvironmentAttribute, bool[]> writingToNodeLocations;
        _currentlyDrawing = true;

        if (_calculating1) {
            writingToStrengths = _strengths1;
            writingToNodeLocations = _nodeLocations1;
        } else {
            writingToStrengths = _strengths2;
            writingToNodeLocations = _nodeLocations2;
        }

        //System.DateTime now = System.DateTime.Now;
        EnvironmentAttribute[] attributes = (EnvironmentAttribute[]) System.Enum.GetValues(typeof(EnvironmentAttribute));
        float[] strengthsAttr;
        float[] rawStrengthsAttr;
        float[] maxStrengthAttr;
        float[] maxNodeStrengthAttr;
        float[] nodeCountsAttr;
        bool[] nodeLocationsAttr;
        foreach (EnvironmentAttribute attr in attributes) {
            strengthsAttr = writingToStrengths[attr];
            rawStrengthsAttr = _rawStrengths[attr];
            maxStrengthAttr = _maxStrength[attr];
            maxNodeStrengthAttr = _maxNodeStrength[attr];
            nodeCountsAttr = _nodeCounts[attr];
            nodeLocationsAttr = writingToNodeLocations[attr];
            for (int x = 0; x < TextureWidth; x++) {
                for (int y = 0; y < TextureHeight; y++) {
                    strengthsAttr[x + y * TextureWidth] = 0;
                    rawStrengthsAttr[x + y * TextureWidth] = 0;
                    maxStrengthAttr[x + y * TextureWidth] = 0;
                    maxNodeStrengthAttr[x + y * TextureWidth] = 0;
                    nodeCountsAttr[x + y * TextureWidth] = 0;
                    nodeLocationsAttr[x + y * TextureWidth] = false;
                }
            }
        }

        //Debug.LogFormat("First time {0}", (System.DateTime.Now - now).Milliseconds);
        //now = System.DateTime.Now;

        float distance = 0;
        int ex = 0, totalEx = 0;
        int pixelX;
        int pixelY;
        //float dx, dy;
        //Vector2 currentPixelPosition;
        Vector2 currentPixelWorldPosition = Vector2.zero;
        int nodesPerFrame = 10;
        int currentNodes = 0;
        int concentration = 0;

        float nodeStrength;
        Vector2 nodeWorldPosition;
        float nodeMaxDistance;

        foreach (EnvironmentAttribute attr in attributes) {
            ex = 0;
            Node[] nodes = Node.GetAllNodes(EnvironmentAttributeConfig.GetConfig(attr).groups).ToArray();
            strengthsAttr = writingToStrengths[attr];
            rawStrengthsAttr = _rawStrengths[attr];
            maxStrengthAttr = _maxStrength[attr];
            maxNodeStrengthAttr = _maxNodeStrength[attr];
            nodeCountsAttr = _nodeCounts[attr];
            nodeLocationsAttr = writingToNodeLocations[attr];

            for (int i = 0; i < nodes.Length; i++) {
                concentration = nodes[i].concentration;
                pixelX = Mathf.RoundToInt(WorldPointToPixel(nodes[i].WorldPosition).x);
                pixelY = Mathf.RoundToInt(WorldPointToPixel(nodes[i].WorldPosition).y);

                if (IsValidLocation(pixelX, pixelY)) {
                    nodeLocationsAttr[pixelX + pixelY * TextureWidth] = true;
                }
                //currentPixelPosition = new Vector2(pixelX, pixelY);

                // Decrease number of nodes and increase range affected

                float strength;
                float difference;
                float higher, lower;
                int radius = 70;
                int minX, maxX, minY, maxY;
                minX = Mathf.Clamp(pixelX - radius, 0, TextureWidth);
                maxX = Mathf.Clamp(pixelX + radius, 0, TextureWidth);
                minY = Mathf.Clamp(pixelY - radius, 0, TextureHeight);
                maxY = Mathf.Clamp(pixelY + radius, 0, TextureHeight);

                nodeStrength = nodes[i].strength;
                nodeWorldPosition = nodes[i].WorldPosition;
                nodeMaxDistance = nodes[i].maxDistance;

                for (int x = minX; x < maxX; x++) {
                    for (int y = minY; y < maxY; y++) {
                        currentPixelWorldPosition.x = PixelXToWorld(x);
                        currentPixelWorldPosition.y = PixelYToWorld(y);
                        ex++;
                        totalEx++;

                        distance = Vector2.Distance(nodeWorldPosition, currentPixelWorldPosition) / nodeMaxDistance;
                        strength = nodeStrength * GetStrength(concentration, distance);
                        if (strength > 0.01f) {

                            if (nodeStrength > maxNodeStrengthAttr[x + y * TextureWidth]) {
                                maxNodeStrengthAttr[x + y * TextureWidth] = nodeStrength;
                            }

                            if (strength > maxStrengthAttr[x + y * TextureWidth]) {
                                maxStrengthAttr[x + y * TextureWidth] = strength;
                            }

                            rawStrengthsAttr[x + y * TextureWidth] += strength;
                            nodeCountsAttr[x + y * TextureWidth] += 1;
                        }

                        //if (strengthsAttr[x + y * TextureWidth] > 0.1f) {
                        //    Debug.LogFormat("{0}, {1} is greater than 3", x, y);
                        //}

                        //if (attr == EnvironmentAttribute.Electromagnetism && attrStrengths[x + y * textureWidth] > 3) {
                        //    Debug.Log(attrStrengths[x + y * textureWidth]);
                        //}
                    }
                }

                currentNodes++;
                if (currentNodes >= nodesPerFrame) {
                    //Debug.LogFormat("Map manager time {0}", (System.DateTime.Now - now).Milliseconds);
                    if (!immediate) {
                        yield return new WaitForEndOfFrame();
                    }
                    //now = System.DateTime.Now;
                    currentNodes = 0;
                }
            }

            for (int x = 0; x < TextureWidth; x++) {
                for (int y = 0; y < TextureHeight; y++) {
                    if (nodeCountsAttr[x + y * TextureWidth] > 0) {
                        //Debug.LogFormat("{0} {1}", rawStrengthsAttr[x + y * TextureWidth], nodeCountsAttr[x + y * TextureWidth]);
                        strengthsAttr[x + y * TextureWidth] = Mathf.Min(maxStrengthAttr[x + y * TextureWidth] + 0.5f * rawStrengthsAttr[x + y * TextureWidth], rawStrengthsAttr[x + y * TextureWidth]);
                        //strengthsAttr[x + y * TextureWidth] = rawStrengthsAttr[x + y * TextureWidth];
                    }
                }
            }
            //Debug.LogFormat("{0} {1} {2}", attr.ToString(), ex, nodes.Length);
        }
        //Debug.LogFormat("Total {0}", totalEx);

        _calculating1 = !_calculating1;
        _currentlyDrawing = false;
        _lastCalculationEndTime = GameTimeManager.GameTime;


        if (!immediate) {
            yield return new WaitForEndOfFrame();
        }
        //Debug.LogFormat("Reached end {0}", GameTimeManager.GameTime);
    }

    // Takes a value between [0, 1]
    private float GetStrength(int concentration, float x) {
        if (x > 1) {
            return 0;
        } else {
            return _memoizedValues[concentration][(int)(x * 100)];
        }
    }

    private float CalculateNormalizedDistributionValue(float delta, float x, float normalize) {
        float numerator = Mathf.Pow(Mathf.Exp(1), -Mathf.Pow(x, 2) / (2 * Mathf.Pow(delta, 2)));
        float denominator = delta * Mathf.Sqrt(2 * Mathf.PI);
        return (numerator / denominator) / normalize;
    }
    
    // This will probably need adjusting to not stretch based on the screen ratio
    public static Vector2 WorldPointToPixel(Vector2 point) {
        return new Vector2(Mathf.RoundToInt((point.x / WeatherManager.BoundsX) * (TextureWidth) + TextureWidth / 2), Mathf.RoundToInt((point.y / WeatherManager.BoundsY) * (TextureHeight) + TextureHeight / 2));
    }

    public static int WorldXToPixel(float x) {
        return Mathf.RoundToInt((x / WeatherManager.BoundsX) * (TextureWidth) + TextureWidth / 2);
    }

    public static int ClampedWorldXToPixel(float x) {
        return Mathf.Clamp(Mathf.RoundToInt((x / WeatherManager.BoundsX) * (TextureWidth) + TextureWidth / 2), 0, TextureWidth - 1);
    }

    public static int WorldZToPixel(float z) {
        return Mathf.RoundToInt((z / WeatherManager.BoundsY) * (TextureHeight) + TextureHeight / 2);
    }

    public static int ClampedWorldZToPixel(float z) {
        return Mathf.Clamp(Mathf.RoundToInt((z / WeatherManager.BoundsY) * (TextureHeight) + TextureHeight / 2), 0, TextureHeight - 1);
    }

    public static Vector3 PixelPointToWorld(int x, int y) {
        return new Vector3(PixelXToWorld(x), 0, PixelYToWorld(y));
    }

    private static float PixelXToWorld(int x) {
        return (x - TextureWidth / 2) * PixelToWorldXRatio;
    }

    private static float PixelYToWorld(int y) {
        return (y - TextureHeight / 2) * PixelToWorldYRatio;
    }


    private bool IsValidLocation(int pixelX, int pixelY) {
        return pixelX >= 0 && pixelY >= 0 && pixelX < TextureWidth && pixelY < TextureHeight;
    }

    public float[] GetStrengths(EnvironmentAttribute attr) {
        if (_calculating1) {
            return _strengths2[attr];
        } else {
            return _strengths1[attr];
        }
    }

    public float GetStrength(EnvironmentAttribute attr, int x, int y) {
        if (_calculating1) {
            return _strengths2[attr][x + y * TextureWidth];
        } else {
            return _strengths1[attr][x + y * TextureWidth];
        }
    }

    public bool[] GetNodeLocations(EnvironmentAttribute attr) {
        if (_calculating1) {
            return _nodeLocations2[attr];
        } else {
            return _nodeLocations1[attr];
        }
    }

    public bool HasUpdatedMapsSince(float time) {
        return _lastCalculationEndTime > time;
    }
}
