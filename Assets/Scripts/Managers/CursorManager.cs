﻿using UnityEngine;
using System.Collections;

/*
    Class that managers the cursor appearance in game. Exposes a few methods so that game elements can trigger a mouse change
*/

public class CursorManager : MonoBehaviour {

    public static Texture2D Normal;
    public static Texture2D Hover;

    public Texture2D normal;
    public Texture2D hover;

	void Awake () {
        Normal = normal;
        Hover = hover;

        TextureScale.Bilinear(Normal, 200, 200);

        SetNormal();
	}

    public static void SetNormal() {
        Cursor.SetCursor(Normal, new Vector2(0, 0), CursorMode.Auto);
    }

    public static void SetHover() {
        Cursor.SetCursor(Hover, new Vector2(0, 0), CursorMode.Auto);
    }
}
