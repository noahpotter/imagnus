﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

/*
    Handles moving weather nodes around, spawning nodes, decaying nodes, displaying nodes and areas for debugging.
*/

[ExecuteInEditMode]
public class WeatherManager : MonoBehaviour {

    public float splitRate;

    public static float BoundsX;
    public static float BoundsY;
    public float boundsX; // How much space the map should show of the world
    public float boundsY; // How much space the map should show of the world

    public float minWeatherSpawnDistance;
    public float maxWeatherSpawnDistance;

    public float minGroupSpeedVarianceModifier;
    public float maxGroupSpeedVarianceModifier;
    public float minDirectionVariance;
    public float maxDirectionVariance;

    private float _lastCalculationEndTime = 0;

    void Awake() {
        BoundsX = boundsX;
        BoundsY = boundsY;
    }

    void Update() {
        Debug.DrawLine(new Vector2(boundsX / -2, boundsY / 2), new Vector2(boundsX / 2, boundsY / 2));
        Debug.DrawLine(new Vector2(boundsX / 2, boundsY / 2), new Vector2(boundsX / 2, boundsY / -2));
        Debug.DrawLine(new Vector2(boundsX / 2, boundsY / -2), new Vector2(boundsX / -2, boundsY / -2));
        Debug.DrawLine(new Vector2(boundsX / -2, boundsY / -2), new Vector2(boundsX / -2, boundsY / 2));

        Debug.DrawLine(new Vector3(boundsX / -2, 0, boundsY / 2), new Vector3(boundsX / 2, 0, boundsY / 2));
        Debug.DrawLine(new Vector3(boundsX / 2, 0, boundsY / 2), new Vector3(boundsX / 2, 0, boundsY / -2));
        Debug.DrawLine(new Vector3(boundsX / 2, 0, boundsY / -2), new Vector3(boundsX / -2, 0, boundsY / -2));
        Debug.DrawLine(new Vector3(boundsX / -2, 0, boundsY / -2), new Vector3(boundsX / -2, 0, boundsY / 2));
    }

    public void Tick() {
        //Debug.Log("Moving weather nodes");
        List<EnvironmentAttributeConfig> configs = EnvironmentAttributeConfig.configs;

        foreach(EnvironmentAttributeConfig config in configs) {
            foreach (Group group in config.groups.ToList()) {
                // Move
                group.Direction = Quaternion.AngleAxis(group.DirectionVariance * GameTimeManager.GameTimePerTick, Vector3.forward) * group.Direction;
                group.Position += group.Direction.normalized * GameTimeManager.GameTimePerTick * (group.Speed + (group.Speed * UnityEngine.Random.Range(-group.SpeedVariance, group.SpeedVariance)));

                // Decay
                List<Node> nodes = Node.GetAllNodes(group);

                foreach (Node node in nodes.ToList()) {
                    node.strength -= (node.decay + (node.decay * UnityEngine.Random.Range(-node.decayVariance, node.decayVariance))) * GameTimeManager.GameTimePerTick;
                    if (node.strength <= 1) {
                        //Debug.Log("Removing node");
                        node.parentGroup.RemoveNode(node);
                    }
                }

                // Check if outside bounds
                float delta = 4;
                if (group.WorldPosition.x > boundsX/2 + maxWeatherSpawnDistance + delta ||
                    group.WorldPosition.x < -boundsX/2 - maxWeatherSpawnDistance - delta ||
                    group.WorldPosition.y > boundsY/2 + maxWeatherSpawnDistance + delta ||
                    group.WorldPosition.y < -boundsY/2 - maxWeatherSpawnDistance - delta) 
                {
                    config.groups.Remove(group);
                }
            }

            // Split
            List<Group> allGroups = Group.GetAllGroups(config.groups);
            foreach (Group group in allGroups.ToList()) {
                float draw = UnityEngine.Random.Range(0f, 1f);

                if (draw < splitRate * GameTimeManager.GameTimePerTick && group.parentGroup != null) {
                    //Debug.Log("Split");
                    Group parentGroup = group.parentGroup;
                    group.RemoveFromParentGroup();
                    group.Position = parentGroup.WorldPosition;
                    group.Speed = parentGroup.Speed;
                    group.SpeedVariance = parentGroup.SpeedVariance;
                    group.Direction = parentGroup.Direction;
                    group.DirectionVariance = GetRandomDirectionVariance();
                    //group.DirectionVariance = parentGroup.DirectionVariance;
                    config.groups.Add(group);
                }
            }

            Group.CleanUpOrphans(config.groups);

            // Spawn in more groups

            foreach(GroupSize groupSize in Enum.GetValues(typeof(GroupSize))) {
                GroupConfig groupConfig = config.GetGroupConfig(groupSize);

                if (UnityEngine.Random.Range(0f, 1f) < groupConfig.spawnChance * GameTimeManager.GameTimePerTick) {
                    //Debug.LogFormat("spawning newgroup {0}", GameTimeManager.GameTime);
                    Vector2 position = GetRandomPositionOutside();
                    Vector2 direction = GetDirectionForPositionOutside(position);
                    float directionVariance = GetRandomDirectionVariance();

                    Group group = Group.GenerateGroup(groupConfig);
                    group.Position = position;
                    group.Direction = direction;

                    group.DirectionVariance = directionVariance;
                    config.groups.Add(group);
                }
            }
        }

        _lastCalculationEndTime = GameTimeManager.GameTime;
    }

    private Vector2 GetRandomPositionOutside() {
        int horizontalOrVertical = UnityEngine.Random.Range(0, 2);
        if (horizontalOrVertical == 0) { // Lets say this represents the horizontal line above and below the game area
            float x = UnityEngine.Random.Range(-(boundsX/2 + maxWeatherSpawnDistance), boundsX/2 + maxWeatherSpawnDistance);
            float y = UnityEngine.Random.Range(boundsY/2 + minWeatherSpawnDistance, boundsY/2 + maxWeatherSpawnDistance) * Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));
            return new Vector2(x, y);
        } else { // This is the vertical lines left and right of the game area 
            float x = UnityEngine.Random.Range(boundsX/2 + minWeatherSpawnDistance, boundsX/2 + maxWeatherSpawnDistance) * Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));
            float y = UnityEngine.Random.Range(-(boundsY/2 + maxWeatherSpawnDistance), boundsY/2 + maxWeatherSpawnDistance);
            return new Vector2(x, y);
        }
    }

    private Vector2 GetDirectionForPositionOutside(Vector2 position) {
        List<Vector2> worldCorners = new List<Vector2> {
            new Vector2(boundsX / 2, boundsY / 2),
            new Vector2(boundsX / 2, boundsY / -2),
            new Vector2(boundsX / -2, boundsY / 2),
            new Vector2(boundsX / -2, boundsY / -2)
        };

        worldCorners = worldCorners.OrderByDescending(x => Vector2.Distance(x, position)).ToList();

        float percentAlongLine = UnityEngine.Random.Range(-1.1f, 1.1f);
        //return ((worldCorners[0] - worldCorners[1]).normalized * percentAlongLine + worldCorners[1]) - position;
        return ((worldCorners[0] - worldCorners[1]) * percentAlongLine + worldCorners[1]) - position;
    }

    private float GetRandomDirectionVariance() {
        return UnityEngine.Random.Range(minDirectionVariance, maxDirectionVariance) * Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));
    }

    private float GetRandomGroupSpeedVariance(float speed) {
        return speed * UnityEngine.Random.Range(minGroupSpeedVarianceModifier, maxGroupSpeedVarianceModifier);
    }

    public bool HasUpdatedMapsSince(float time) {
        return _lastCalculationEndTime > time;
    }
}
