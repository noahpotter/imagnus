﻿using UnityEngine;
using System.Collections;

/*
    Separating real time from game time is important in order to simulate and test weather patterns and resource generation
*/

public class GameTimeManager : MonoBehaviour {

    public static float GameTime {
        get {
            return CurrentTick * GameTimePerTick;
        }
    }

    public static float TickRate = 60; // How many ticks per second
    public static float GameTimePerTick = 1 / 60f; // How many seconds each tick represents
    private static float TimePerTick; // How many real seconds each tick represents
    public static int CurrentTick = 0;

    private static float MinTickRate = 1;
    private static float MaxTickRate = 200;

    public int startingWeatherTicks;
    public int startingMapTicks;

    private WeatherManager _weatherManager;
    private MapManager _mapManager;
    private ResourceGenerator _resourceGenerator;

    private float _lastTick = 0;

    void Awake() {
        CalculateNewRates();
        _weatherManager = GetComponent<WeatherManager>();
        _mapManager = GetComponent<MapManager>();
        _resourceGenerator = GetComponent<ResourceGenerator>();
    }

	void Start () {
        int currentWeatherTick = 0;
        while (currentWeatherTick < startingWeatherTicks) {
            _weatherManager.Tick();
            currentWeatherTick++;
        }

        int currentMapTick = 0;
        while (currentMapTick < startingMapTicks) {
            ImmediateTick();
            currentMapTick++;
        }
    }

    void FixedUpdate() {
        while (Time.time - _lastTick > TimePerTick) {
            Tick();
        }
    }

    private void Tick() {
        CurrentTick += 1;

        _weatherManager.Tick();
        _mapManager.Tick();
        _resourceGenerator.Tick();

        _lastTick += TimePerTick;
    }

    private void ImmediateTick() {
        CurrentTick += 1;

        _weatherManager.Tick();
        _mapManager.Tick(true);
        _resourceGenerator.Tick();
    }

    public static void IncreaseSpeed() {
        TickRate = Mathf.Clamp(TickRate + 5, MinTickRate, MaxTickRate);
        CalculateNewRates();
    }

    public static void DecreaseSpeed() {
        TickRate = Mathf.Clamp(TickRate - 5, MinTickRate, MaxTickRate);
        CalculateNewRates();
    }

    private static void CalculateNewRates() {
        TimePerTick = 1 / TickRate;
    }
}
