﻿
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public enum Windows {
    Map,
    Encyclopedia,
    Buildings
}

/*
    Describes the configuration for a specific window
*/

[System.Serializable]
public class WindowConfig {
    public Windows window;
    public GameObject contentPrefab;
    public string title;
    public float minWidth;
    public float minHeight;

    private GameObject _content;
    private GameObject _window;

    private bool _visible;
    private bool _overrideVisible;
    private bool _useOverride;

    public void Init(WindowManager windowManager, GameObject windowPrefab, Canvas canvas, GameObject player) {
        GameObject windowView = GameObject.Instantiate(windowPrefab);

        _content = GameObject.Instantiate(contentPrefab);
        _content.transform.SetParent(windowView.transform.Find("Content"), false);

        windowView.transform.SetParent(canvas.transform, false);
        windowView.transform.Find("Toolbar").Find("Title").GetComponentInChildren<Text>().text = title;
        windowView.BroadcastMessage("SetPlayer", player, SendMessageOptions.DontRequireReceiver);

        UIWindow uiWindow = windowView.GetComponent<UIWindow>();
        uiWindow.windowManager = windowManager;
        uiWindow.minWidth = minWidth;
        uiWindow.minHeight = minHeight;
        uiWindow.windowType = window;
        uiWindow.canvas = canvas;

        _window = windowView;

        HideWindow();
    }

    public void ToggleVisibility() {
        if (_visible) {
            HideWindow();
        } else {
            ShowWindow();
        }
    }

    public void ShowWindow() {
        _window.transform.SetAsLastSibling();
        _visible = true;
        ReflectVisibility();
    }

    public void HideWindow() {
        _visible = false;
        ReflectVisibility();
    }

    public void EnableOverride(bool isVisible) {
        _overrideVisible = isVisible;
        _useOverride = true;
        ReflectVisibility();
    }

    public void DisableOverride() {
        _useOverride = false;
        ReflectVisibility();
    }

    private void ReflectVisibility() {
        if (_useOverride) {
            _window.SetActive(_overrideVisible);
        } else {
            _window.SetActive(_visible);
        }
    }
}

/*
    Handles creating the windows, showing them and hiding them
*/

public class WindowManager : MonoBehaviour {

    public GameObject windowPrefab;
    public WindowConfig[] windowConfigs;

    [HideInInspector]
    public GameObject player;

    private Canvas _canvas;

    void Awake() {
        _canvas = GetComponent<Canvas>();
    }

    void Start() {
        for (int i = 0; i < windowConfigs.Length; i++) {
            windowConfigs[i].Init(this, windowPrefab, _canvas, player);
        }
    }

    public void ShowWindow(Windows window) {
        windowConfigs.ToList().Find(x => x.window == window).ShowWindow();
    }

    public void HideWindow(Windows window) {
        windowConfigs.ToList().Find(x => x.window == window).HideWindow();
    }

    public void ToggleWindow(Windows window) {
        windowConfigs.ToList().Find(x => x.window == window).ToggleVisibility();
    }

    public void HideWindows() {
        windowConfigs.ToList().ForEach(x => x.EnableOverride(false));
    }

    public void ShowWindows() {
        windowConfigs.ToList().ForEach(x => x.DisableOverride());
    }
}
