﻿using UnityEngine;
using System.Collections;

public enum Colors {
    Primary,
    PrimaryShadow,
    Secondary,
    SecondaryShadow,
    Tertiary
}

public class ColorManager : MonoBehaviour {

    public Color primaryColor;
    public Color primaryShadowColor;
    public Color secondaryColor;
    public Color secondaryShadowColor;
    public Color tertiaryColor;

    public static Color PrimaryColor;
    public static Color PrimaryShadowColor;
    public static Color SecondaryColor;
    public static Color SecondaryShadowColor;
    public static Color TertiaryColor;

    void Awake() {
        PrimaryColor = primaryColor;
        PrimaryShadowColor = primaryShadowColor;
        SecondaryColor = secondaryColor;
        SecondaryShadowColor = secondaryShadowColor;
        TertiaryColor = tertiaryColor;
    }

    public static Color GetColor(Colors color) {
        switch(color) {
            case Colors.Primary: return PrimaryColor;
            case Colors.PrimaryShadow: return PrimaryShadowColor;
            case Colors.Secondary: return SecondaryColor;
            case Colors.SecondaryShadow: return SecondaryShadowColor;
            case Colors.Tertiary: return TertiaryColor;
            default: return PrimaryColor;
        }
    }
}