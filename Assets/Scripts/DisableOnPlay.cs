﻿using UnityEngine;
using System.Collections;

public class DisableOnPlay : MonoBehaviour {
	void Awake () {
        gameObject.SetActive(false);
	}
}
