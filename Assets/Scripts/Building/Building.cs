﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;

/*
    Handles the spawned building. Takes actions, keeps track of the building level, keeps references to it's configuration, creates an upgrade menu
*/

public class Building : MonoBehaviour, IPointerDownHandler {

    public BuildingConfig buildingConfig;
    public GameObject buildingPrefab;

    private GameObject _player;
    private bool _menuOpen;

    private float _menuDistance = 4;

    public int level = 1;
    private int _maxLevel;
    private GameObject _upgradeWindow;

    private GameObject _menu;

    void Start() {
        _maxLevel = buildingConfig.upgradeRequirements.Length + 1;
        _menu = Instantiate(BuildingsConfig.MenuPrefab);
        _menu.GetComponent<CanvasBillboard>().target = _player.GetComponent<PlayerCamera>().camera.gameObject;
        _menu.GetComponent<Canvas>().worldCamera = _player.GetComponent<PlayerCamera>().buildingUICamera;

        //_menu.transform.SetParent(transform, false);

        _upgradeWindow = UIBuildingsManager.CreateUpgradeView(this, level + 1);
        _upgradeWindow.transform.SetParent(_menu.transform, false);

        _menuOpen = false;
    }

    void Update() {
        if (_menuOpen && !PlayerWithinRangeOfMenu()) {
            _menuOpen = false;
        }

        ReflectMenuVisibility();
    }

    public void SetPlayer(GameObject player) {
        _player = player;
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Left) {
            if (PlayerWithinRangeOfMenu()) {
                if (level < _maxLevel) {
                    _menuOpen = true;
                    _menu.transform.position = eventData.pointerCurrentRaycast.worldPosition;
                }
            }
        }
    }

    public void Upgrade() {
        level += 1;
        BuildingConfig.UpdateModel(this);

        if (level < GetMaxLevel()) {
            CreateUpgradeMenu(level + 1);
        } else {
            _menuOpen = false;
            _menu.SetActive(false);

            if (buildingConfig.type == BuildingTypes.Obelisk) {
                // Obelisk is fully upgraded, end the game
                SceneManager.LoadScene("End Game");
            }
        }
    }

    private void CreateUpgradeMenu(int nextLevel) {
        if (_upgradeWindow) {
            Destroy(_upgradeWindow);
        }

        if (nextLevel <= _maxLevel) {
            _upgradeWindow = UIBuildingsManager.CreateUpgradeView(this, nextLevel);
            _upgradeWindow.transform.SetParent(_menu.transform, false);
        }
    }

    private void ReflectMenuVisibility() {
        _menu.SetActive(_menuOpen);
    }

    private bool PlayerWithinRangeOfMenu() {
        return Vector3.Distance(transform.position, _player.transform.position) <= _menuDistance;
    }

    private int GetMaxLevel() {
        return buildingConfig.upgradeRequirements.Max(x => x.level);
    }
}
