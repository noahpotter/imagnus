﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class BuildingRequirement {
    public SpawnableResources resource;
    public int quantity;
}

[System.Serializable]
public class UpgradeRequirement {
    public int level;
    public BuildingRequirement[] requirements;
}

public enum BuildingTypes {
    Sensor,
    Obelisk
}

/*
    Describes a building, including building requirements, upgrade requirements, models, and what type it is. 
    The static class provides methods that operate on a building config or an actual building such as creating the building from config, getting a list of building requirements
    and checking if a position is valid for a building
*/

public class BuildingConfig : MonoBehaviour {

    public static List<BuildingConfig> BuildingConfigs = new List<BuildingConfig>();
    public static float BuildRadius = 5;

    public BuildingTypes type;
    public BuildingRequirement[] requirements;
    public UpgradeRequirement[] upgradeRequirements;
    public GameObject[] models;
    public GameObject placementModel;
    
    void Start() {
        GameObject player = GameObject.Find("Player");
        Building building = Build(this, transform.position, Quaternion.identity, player);
        player.GetComponent<PlayerBuildingManager>().AddBuilding(building);

    }

    public static bool PlayerCanBuild(BuildingConfig buildingConfig, PlayerInventory playerInventory) {
        foreach(BuildingRequirement buildingRequirement in buildingConfig.requirements) {
            if (!playerInventory.HasEnough(buildingRequirement.resource, buildingRequirement.quantity)) {
                return false;
            }
        }
        return true;
    }
    public static bool PlayerCanUpgrade(Building building, PlayerInventory playerInventory) {
        //List<BuildingRequirement> buildingRequirements
        foreach (BuildingRequirement buildingRequirement in GetUpgradeRequirements(building)) {
            if (!playerInventory.HasEnough(buildingRequirement.resource, buildingRequirement.quantity)) {
                return false;
            }
        }
        return true;
    }

    public static GameObject CreatePlacementModel(BuildingConfig building) {
        GameObject placementModel = Instantiate(building.placementModel);
        placementModel.ChangeLayers(LayerMask.NameToLayer("Placement"));
        return placementModel;
    }

    public static bool CanBuildHere(BuildingConfig building, Vector3 position) {
        BoxCollider collider = building.placementModel.transform.Find("Spawn Block").GetComponent<BoxCollider>();
        bool didCollide = Physics.CheckBox(position, new Vector3(collider.size.x / 2, 100, collider.size.z / 2), Quaternion.identity, LayerMask.GetMask("Spawn Block"), QueryTriggerInteraction.Collide);
        float distance = Vector3.Distance(position, GameObject.Find("Player").transform.position);
        return !didCollide && distance <= BuildRadius;
    }

    public static List<BuildingRequirement> GetUpgradeRequirements(Building building) {
        return building.buildingConfig.upgradeRequirements.ToList().Find(x => x.level == building.level + 1).requirements.ToList();
    }

    public static Building Build(BuildingConfig buildingConfig, Vector3 position, Quaternion rotation, GameObject player) {
        GameObject newBuilding = new GameObject();
        newBuilding.name = buildingConfig.type.GetDescription();
        newBuilding.transform.position = position;
        newBuilding.transform.rotation = rotation;

        GameObject b = Instantiate(buildingConfig.models[0]) as GameObject;
        b.name = "Model";
        b.transform.SetParent(newBuilding.transform);
        b.transform.localPosition = Vector2.zero;
        b.transform.localRotation = Quaternion.identity;

        Building building = newBuilding.AddComponent<Building>();
        building.SetPlayer(player);
        building.buildingConfig = buildingConfig;
        return building;
    }

    public static void UpdateModel(Building building) {
        if (building.buildingConfig.models.Length >= building.level) {
            Destroy(building.transform.Find("Model").gameObject);

            GameObject b = Instantiate(building.buildingConfig.models[building.level - 1]) as GameObject;
            b.name = "Model";
            b.transform.SetParent(building.transform);
            b.transform.localPosition = Vector2.zero;
        }
    }

    public static BuildingRequirement[] GetUpgradeRequirements(BuildingConfig buildingConfig, int level) {
        UpgradeRequirement upgradeRequirement = buildingConfig.upgradeRequirements.ToList().Find(x => x.level == level);
        if (upgradeRequirement != null) {
            return upgradeRequirement.requirements;
        } else {
            return null;
        }
    }
}
