﻿using UnityEngine;
using UnityEngine.EventSystems;

/*
    Connects a spawned resource to its configuration and handles adding to a players inventory if clicked on
*/

public class SpawnedResource : MonoBehaviour, IPointerDownHandler {

    public Resource resource;
    public float spawnedTime;
    public float lifetime;

    public void Destroy() {
        if (gameObject) {
           GameObject.Destroy(gameObject);
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Left && Vector3.Distance(transform.position, GameObject.Find("Player").transform.position) < ResourceGenerator.GatherRadius) {
            PlayerInventory playerInventory = FindObjectOfType<PlayerInventory>();
            playerInventory.AddToInventory(resource.resource, 1);
            Destroy();
        }
    }
}
