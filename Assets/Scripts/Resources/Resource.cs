﻿using UnityEngine;
using System.Collections.Generic;
using System.ComponentModel;

public enum SpawnableResources {
    Chestnut,
    Teak,
    [Description("White Oak")]
    WhiteOak,
    [Description("Red Mahogany")]
    RedMahogany,
    Wheat,
    Barley,
    Oat,
    Flax,
    Tin,
    Copper,
    Iron,
    Tungsten,
    Amethist,
    Emerald,
    Topaz,
    Obsidian
}

public enum ResourceTypes {
    Wood,
    Plant,
    Ore,
    Crystal
}

/*
    Holds configuration information for each resource
*/

public class Resource : MonoBehaviour {

    public static List<Resource> GameResources;

    public SpawnableResources resource;
    public ResourceTypes resourceType;
    public Sprite sprite;
    public GameObject model;
    public BoxCollider spawnBlock;

    public int spawnLimit;
    public float minLifetime;
    public float maxLifetime;
    public float spawnChance; // Chance of spawning every second

    public bool useAirPressure;
    public bool useElectromagnetism;
    public bool useHumidity;
    public bool usePrecipitation;
    public bool useSunExposure;
    public bool useTemperature;
    public bool useWindSpeed;

    public float minAirPressure;
    public float maxAirPressure;

    public float minElectromagnetism;
    public float maxElectromagnetism;

    public float minHumidity;
    public float maxHumidity;

    public float minPrecipitation;
    public float maxPrecipitation;

    public float minSunExposure;
    public float maxSunExposure;

    public float minTemperature;
    public float maxTemperature;

    public float minWindSpeed;
    public float maxWindSpeed;

    void Start() {
        ResourceGenerator resourceGenerator = FindObjectOfType<ResourceGenerator>();
        resourceGenerator.SpawnResource(this, MapManager.WorldPointToPixel(transform.position));
    }

    public static Resource GetResource(SpawnableResources spawnableResource) {
        return GameResources.Find(x => x.resource == spawnableResource);
    }
}
