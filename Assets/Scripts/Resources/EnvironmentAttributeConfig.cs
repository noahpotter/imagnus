﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System;
using System.Reflection;

/*
    Keeps track of each environment attribute configuration
*/

public enum EnvironmentAttribute {
    [Description("Air Pressure")]
    AirPressure = 0,
    Electromagnetism = 1,
    Humidity = 2,
    Precipitation = 3,
    [Description("Sun Exposure")]
    SunExposure = 4,
    Temperature = 5,
    [Description("Wind Speed")]
    WindSpeed = 6
}

public static class EnumExtensions {
    public static string GetDescription<T>(this T value) {
        Type type = value.GetType();
        string name = Enum.GetName(type, value);
        if (name != null) {
            FieldInfo field = type.GetField(name);
            if (field != null) {
                DescriptionAttribute attr =
                       Attribute.GetCustomAttribute(field,
                         typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attr != null) {
                    return attr.Description;
                }
            }
        }
        return value.ToString();
    }
}

public class EnvironmentAttributeConfig : MonoBehaviour {

    public static List<EnvironmentAttributeConfig> configs = new List<EnvironmentAttributeConfig>();

    public EnvironmentAttribute attribute;
    public GroupConfig[] groupConfigs;
    public List<Group> groups;

    public GroupConfig GetGroupConfig(GroupSize groupSize) {
        return groupConfigs.ToList().First(x => x.groupSize == groupSize);
    }

    void Awake() {
        groups = new List<Group>();
        configs.Add(this);
    }

    public static EnvironmentAttributeConfig GetConfig(EnvironmentAttribute attribute) {
        return configs.First(x => x.attribute == attribute);
    }

    public static EnvironmentAttributeConfig GetConfig(PlayerUI playerUI) {
        return configs.First(x => x.attribute == playerUI.currentAttribute);
    }
}
