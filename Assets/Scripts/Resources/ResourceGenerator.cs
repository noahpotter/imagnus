﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
    Generates resources based on current weather conditions
*/

public class ResourceGenerator : MonoBehaviour {

    public static List<SpawnedResource> SpawnedResources = new List<SpawnedResource>();
    public static float GatherRadius = 3; // Could put this somewhere else

    public int maxSpawnPerTick;

    private static Dictionary<SpawnableResources, int> _spawnedResourceCount = new Dictionary<SpawnableResources, int>();

    private Resource[] _gameResourcesArr;
    private MapManager _mapManager;
    private WeatherManager _weatherManager;
    private float _lastUpdated = 0;
    private Dictionary<Resource, List<Vector2>> _validSpawnPositions = new Dictionary<Resource, List<Vector2>>();

    void Awake() {
        Resource.GameResources = Resources.LoadAll<Resource>("Resource Prefabs").ToList();
        Debug.Log(Resource.GameResources.Count);
        _mapManager = FindObjectOfType<MapManager>();
        _weatherManager = FindObjectOfType<WeatherManager>();

        _gameResourcesArr = Resource.GameResources.ToArray();

        foreach (Resource resource in Resource.GameResources) {
            _spawnedResourceCount.Add(resource.resource, 0);
            _validSpawnPositions.Add(resource, new List<Vector2>());
        }
    }
	
	public void Tick () {
        // Despawn any spawned resources if it's been too long
        foreach (SpawnedResource spawnedResource in SpawnedResources.ToList()) {
            if (spawnedResource != null) {
                if (GameTimeManager.GameTime - spawnedResource.spawnedTime > spawnedResource.lifetime) {
                    DespawnResource(spawnedResource);
                }
            }
        }

        // Check each map after every update
        Resource r;
        float airPressureValue, electromagnetismValue, humidityValue, precipitationValue, sunExposureValue, temperatureValue, windSpeedValue;
        if (_mapManager.HasUpdatedMapsSince(_lastUpdated)) {
            //Debug.Log("Has maps");
            foreach (Resource resource in Resource.GameResources) {
                _validSpawnPositions[resource].Clear();
            }

            _lastUpdated = GameTimeManager.GameTime;
            float[] airPressureStrengths = _mapManager.GetStrengths(EnvironmentAttribute.AirPressure);
            float[] electromagnetismStrengths = _mapManager.GetStrengths(EnvironmentAttribute.Electromagnetism);
            float[] humidityStrengths = _mapManager.GetStrengths(EnvironmentAttribute.Humidity);
            float[] precipitationStrengths = _mapManager.GetStrengths(EnvironmentAttribute.Precipitation);
            float[] sunExposureStrengths = _mapManager.GetStrengths(EnvironmentAttribute.SunExposure);
            float[] temperatureStrengths = _mapManager.GetStrengths(EnvironmentAttribute.Temperature);
            float[] windSpeedStrengths = _mapManager.GetStrengths(EnvironmentAttribute.WindSpeed);

            for (int x = 0; x < MapManager.TextureWidth; x++) {
                for (int y = 0; y < MapManager.TextureHeight; y++) {
                    // Mwahahaha
                    airPressureValue = airPressureStrengths[x + y * MapManager.TextureWidth];
                    electromagnetismValue = electromagnetismStrengths[x + y * MapManager.TextureWidth];
                    humidityValue = humidityStrengths[x + y * MapManager.TextureWidth];
                    precipitationValue = precipitationStrengths[x + y * MapManager.TextureWidth];
                    sunExposureValue = sunExposureStrengths[x + y * MapManager.TextureWidth];
                    temperatureValue = temperatureStrengths[x + y * MapManager.TextureWidth];
                    windSpeedValue = windSpeedStrengths[x + y * MapManager.TextureWidth];

                    // Could optimize to avoid checking resources that have already spawned enough this update
                    for (int i = 0; i < _gameResourcesArr.Length; i++) {
                        r = _gameResourcesArr[i];
                        if (
                            (r.useAirPressure || r.useElectromagnetism || r.useHumidity || r.usePrecipitation || r.useSunExposure || r.useTemperature || r.useWindSpeed) &&
                            (!r.useAirPressure || (r.useAirPressure && r.minAirPressure <= airPressureValue && airPressureValue <= r.maxAirPressure)) &&
                            (!r.useElectromagnetism || (r.useElectromagnetism && r.minElectromagnetism <= electromagnetismValue && electromagnetismValue <= r.maxElectromagnetism)) &&
                            (!r.useHumidity || (r.useHumidity && r.minHumidity <= humidityValue && humidityValue <= r.maxHumidity)) &&
                            (!r.usePrecipitation || (r.usePrecipitation && r.minPrecipitation <= precipitationValue && precipitationValue <= r.maxPrecipitation)) &&
                            (!r.useSunExposure || (r.useSunExposure && r.minSunExposure <= sunExposureValue && sunExposureValue <= r.maxSunExposure)) &&
                            (!r.useTemperature || (r.useTemperature && r.minTemperature <= temperatureValue && temperatureValue <= r.maxTemperature)) &&
                            (!r.useWindSpeed || (r.useWindSpeed && r.minWindSpeed <= windSpeedValue && windSpeedValue <= r.maxWindSpeed))
                        ) {
                            if (RollSpawn(r) && CanSpawnHere(r, x, y)) {
                                _validSpawnPositions[r].Add(new Vector2(x, y));
                            }
                        }
                    }
                }
            }
            //Debug.LogFormat("Resource generator time {0}", (System.DateTime.Now - now).Milliseconds);

            int spawnedThisTick = 0;
            foreach (Resource resource in Resource.GameResources) {
                while (spawnedThisTick <= maxSpawnPerTick && _validSpawnPositions[resource].Count > 0 && _spawnedResourceCount[resource.resource] < resource.spawnLimit) {
                    _validSpawnPositions[resource].Shuffle();
                    SpawnResource(resource, Mathf.RoundToInt(_validSpawnPositions[resource][0].x), Mathf.RoundToInt(_validSpawnPositions[resource][0].y));
                    spawnedThisTick++;
                    _validSpawnPositions[resource].Remove(_validSpawnPositions[resource][0]);
                }
            }
        }
    }

    public bool RollSpawn(Resource resource) {
        return Random.Range(0f, 1f) < resource.spawnChance * GameTimeManager.GameTimePerTick;
    }

    public bool CanSpawnHere(Resource resource, int pixelX, int pixelY) {
        Vector3 position = MapManager.PixelPointToWorld(pixelX, pixelY);
        bool didCollide = Physics.CheckBox(position, new Vector3(resource.spawnBlock.size.x / 2, 100, resource.spawnBlock.size.z / 2), Quaternion.identity, LayerMask.GetMask("Spawn Block"), QueryTriggerInteraction.Collide);

        return !didCollide;
    }

    public void SpawnResource(Resource resource, Vector2 pixelPosition) {
        SpawnResource(resource, Mathf.RoundToInt(pixelPosition.x), Mathf.RoundToInt(pixelPosition.y));
    }

    public void SpawnResource(Resource resource, int pixelX, int pixelY) {
        Vector3 position = MapManager.PixelPointToWorld(pixelX, pixelY);
        RaycastHit hit;
        bool didHit = Physics.Raycast(new Ray(new Vector3(position.x, 100, position.z), Vector3.down), out hit, 200, LayerMask.GetMask("Floor"));
        if (didHit) {
            position = hit.point - new Vector3(0, 0.1f, 0);
            _spawnedResourceCount[resource.resource] += 1;

            GameObject go = new GameObject();
            go.transform.position = position;

            GameObject model = Instantiate(resource.model);
            model.transform.SetParent(go.transform, false);
            model.transform.localPosition = Vector3.zero;
            model.transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);

            SpawnedResource spawnedResource = go.AddComponent<SpawnedResource>();
            spawnedResource.lifetime = Random.Range(resource.minLifetime, resource.maxLifetime);
            spawnedResource.resource = resource;
            spawnedResource.spawnedTime = GameTimeManager.GameTime;
            SpawnedResources.Add(spawnedResource);
        }
    }

    public void DespawnResource(SpawnedResource spawnedResource) {
        _spawnedResourceCount[spawnedResource.resource.resource] -= 1;
        spawnedResource.Destroy();
        SpawnedResources.Remove(spawnedResource);
    }
}
