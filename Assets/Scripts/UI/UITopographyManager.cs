﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public class MapIcon {
    public SpawnedResource spawnedResource;
    public GameObject uiSprite;

    public void Destroy() {
        GameObject.Destroy(uiSprite);
    }
}

/*
    Displays the players maps.
    Handles drawing the maps with resources, sensors, and weather nodes.
    Holds a list of previous maps and handles switching between them.
*/

public class UITopographyManager : MonoBehaviour {

    public bool shouldDraw;
    public bool shouldDrawResources;
    public bool useSensors;
    public bool showNodes;
    public GameObject topography;
    public GameObject iconsContainer;
    public GameObject playerPositionIndicatorPrefab;

    public GameObject uiSpritePrefab;

    private Texture2D _texture;
    private Color _blankColor;

    private MapManager _mapManager;
    private WeatherManager _weatherManager;
    private UIMapManager _uiMapManager;
    private PlayerUI _playerUI;
    private GameObject _playerGameObject;
    private GameObject _playerPositionIndicator;
    private PlayerBuildingManager _playerBuildingManager;

    private float _lastDraw = -100;

    private Dictionary<EnvironmentAttribute, List<Color32[]>> _mapSnapshots = new Dictionary<EnvironmentAttribute, List<Color32[]>>();
    public int SnapshotCount { get { return _mapSnapshots.First().Value.Count; } }

    private List<MapIcon> _mapIcons = new List<MapIcon>();

    private bool[] _visibleArea;

    private EnvironmentAttribute _lastDrawnAttributeMap;

    //private bool _currentlyDrawing = false;

    void Start() {
        _uiMapManager = transform.GetComponent<UIMapManager>();
        _mapManager = FindObjectOfType<MapManager>();
        _weatherManager = FindObjectOfType<WeatherManager>();

        _visibleArea = new bool[MapManager.TextureHeight * MapManager.TextureWidth];

        _blankColor = _mapManager.blankColor;

        _texture = new Texture2D(MapManager.TextureWidth, MapManager.TextureHeight);
        _texture.filterMode = FilterMode.Point;
        topography.GetComponent<Image>().preserveAspect = true;
        topography.GetComponent<Image>().sprite = Sprite.Create(_texture, new Rect(0, 0, MapManager.TextureWidth, MapManager.TextureHeight), Vector2.zero);

        topography.transform.parent.GetComponent<AspectRatioFitter>().aspectRatio = MapManager.AspectRatio;

        foreach(EnvironmentAttribute attribute in System.Enum.GetValues(typeof(EnvironmentAttribute))) {
            _mapSnapshots.Add(attribute, new List<Color32[]>());
        }
    }

    void Update() {
        if (_mapManager.HasUpdatedMapsSince(_lastDraw)) {
            foreach (EnvironmentAttribute attribute in System.Enum.GetValues(typeof(EnvironmentAttribute))) {
                //StartCoroutine(DrawMap(attribute));
                DrawMap(attribute);
            }
            _uiMapManager.AddedSnapshot();
            _lastDraw = GameTimeManager.GameTime;
        }

        if (shouldDraw) {
            DrawPlayer();

            if (shouldDrawResources) {
                DrawResources();
            }

            UpdateVisibleArea();
            ShowSnapshot();
        }

        // Un comment this to see map and node directions
        Debug.DrawLine(new Vector2(WeatherManager.BoundsX / -2, WeatherManager.BoundsY / 2), new Vector2(WeatherManager.BoundsX / 2, WeatherManager.BoundsY / 2));
        Debug.DrawLine(new Vector2(WeatherManager.BoundsX / 2, WeatherManager.BoundsY / 2), new Vector2(WeatherManager.BoundsX / 2, WeatherManager.BoundsY / -2));
        Debug.DrawLine(new Vector2(WeatherManager.BoundsX / 2, WeatherManager.BoundsY / -2), new Vector2(WeatherManager.BoundsX / -2, WeatherManager.BoundsY / -2));
        Debug.DrawLine(new Vector2(WeatherManager.BoundsX / -2, WeatherManager.BoundsY / -2), new Vector2(WeatherManager.BoundsX / -2, WeatherManager.BoundsY / 2));

        Node[] nodes = Node.GetAllNodes(EnvironmentAttributeConfig.GetConfig(_playerUI).groups).ToArray();

        for (int i = 0; i < nodes.Length; i++) {
            Debug.DrawRay(nodes[i].WorldPosition, nodes[i].parentGroup.Direction.normalized * 100);
            //Debug.Log(nodes[i].strength);
        }
    }

    // Use two textures that are switched and split across multiple frames
    void DrawMap(EnvironmentAttribute attribute) {
        Color32[] snapshot = new Color32[MapManager.TextureWidth * MapManager.TextureHeight];
        //_currentlyDrawing = true;

        float[] strengths = _mapManager.GetStrengths(attribute);
        bool[] nodeLocations = _mapManager.GetNodeLocations(attribute);

        for (int x = 0; x < MapManager.TextureWidth; x++) {
            for (int y = 0; y < MapManager.TextureHeight; y++) {
                if (_visibleArea[x + y * MapManager.TextureWidth] && strengths[x + y * MapManager.TextureWidth] > 3f) {
                    if (showNodes && nodeLocations[x + y * MapManager.TextureWidth]) {
                        snapshot[x + y * MapManager.TextureWidth] = _uiMapManager.Colors[_uiMapManager.Colors.Length - 1];
                    } else {
                        snapshot[x + y * MapManager.TextureWidth] = _uiMapManager.Colors[Mathf.Clamp(Mathf.FloorToInt(strengths[x + y * MapManager.TextureWidth] / _mapManager.segmentRange), 0, 100 / _mapManager.segmentRange)];
                    }
                } else {
                    snapshot[x + y * MapManager.TextureWidth] = _blankColor;
                }
            }
        }
        _mapSnapshots[attribute].Insert(0, snapshot);

        //yield return new WaitForEndOfFrame();
    }

    public void ShowSnapshot() {
        if (_uiMapManager != null && _mapSnapshots[_playerUI.currentAttribute].Count >= _uiMapManager.CurrentSnapshot) {
            Color32[] snapshot = _mapSnapshots[_playerUI.currentAttribute][_uiMapManager.CurrentSnapshot - 1];
            _texture.SetPixels32(snapshot);
            _texture.Apply();
        }
    }

    private void UpdateVisibleArea() {
        if (useSensors) {
            for (int x = 0; x < MapManager.TextureWidth; x++) {
                for (int y = 0; y < MapManager.TextureHeight; y++) {
                    _visibleArea[x + y * MapManager.TextureWidth] = false;
                }
            }

            List<Building> sensors = _playerBuildingManager.GetSensors();

            int minX, maxX, minZ, maxZ;

            // How many world units to uncover
            float[] sensorRadii = new float[] { 10, 15, 20 };

            foreach (Building sensor in sensors) {
                minX = MapManager.ClampedWorldXToPixel(sensor.transform.position.x - sensorRadii[sensor.level - 1]);
                maxX = MapManager.ClampedWorldXToPixel(sensor.transform.position.x + sensorRadii[sensor.level - 1]);
                minZ = MapManager.ClampedWorldZToPixel(sensor.transform.position.z - sensorRadii[sensor.level - 1]);
                maxZ = MapManager.ClampedWorldZToPixel(sensor.transform.position.z + sensorRadii[sensor.level - 1]);

                Vector3 sensorPosition = new Vector3(sensor.transform.position.x, 0, sensor.transform.position.z);

                for (int x = minX; x <= maxX; x++) {
                    for (int z = minZ; z <= maxZ; z++) {
                        if (!_visibleArea[x + z * MapManager.TextureWidth]) {
                            _visibleArea[x + z * MapManager.TextureWidth] = Vector3.Distance(sensorPosition, MapManager.PixelPointToWorld(x, z)) <= sensorRadii[sensor.level - 1];
                        }
                    }
                }
            }
        } else {
            for (int x = 0; x < MapManager.TextureWidth; x++) {
                for (int y = 0; y < MapManager.TextureHeight; y++) {
                    _visibleArea[x + y * MapManager.TextureWidth] = true;
                }
            }
        }
    }

    private void DrawPlayer() {
        if (!_playerPositionIndicator) {
            _playerPositionIndicator = Instantiate(playerPositionIndicatorPrefab);
            _playerPositionIndicator.transform.SetParent(iconsContainer.transform, false);
        }


        _playerPositionIndicator.transform.localPosition = WorldPointToMap(_playerGameObject.transform.position, topography);
    }

    private void DrawResources() {
        List<SpawnedResource> spawnedResources = ResourceGenerator.SpawnedResources;

        foreach (SpawnedResource spawnedResource in spawnedResources.ToList()) {
            if (spawnedResource == null) {
                spawnedResources.Remove(spawnedResource);
            }
        }

        foreach (MapIcon mapIcon in _mapIcons.ToList()) {
            if (spawnedResources.Find(x => x == mapIcon.spawnedResource) == null) {
                mapIcon.Destroy();
                _mapIcons.Remove(mapIcon);
            }
        }

        foreach (SpawnedResource spawnedResource in spawnedResources) {
            if (_mapIcons.Find(x => x.spawnedResource == spawnedResource) == null) {
                // We don't have an icon yet for this resource, make one
                _mapIcons.Add(CreateIcon(spawnedResource));
            }
        }
    }

    private MapIcon CreateIcon(SpawnedResource spawnedResource) {
        GameObject uiSprite = Instantiate(uiSpritePrefab);
        uiSprite.transform.SetParent(iconsContainer.transform, false);
        uiSprite.transform.position = Vector2.zero;
        uiSprite.transform.localPosition = Vector2.zero;
        uiSprite.GetComponent<Image>().sprite = spawnedResource.resource.sprite;
        uiSprite.transform.localPosition = WorldPointToMap(spawnedResource.transform.position, topography);
        return new MapIcon { spawnedResource = spawnedResource, uiSprite = uiSprite };
    }

    public void WindowResized() {
        foreach (SpawnedResource spawnedResource in ResourceGenerator.SpawnedResources) {
            MapIcon mapIcon = _mapIcons.Find(x => x.spawnedResource == spawnedResource);
            if (mapIcon != null) {
                mapIcon.uiSprite.transform.localPosition = WorldPointToMap(spawnedResource.transform.position, topography);
            }
        }
    }

    public static Vector2 WorldPointToMap(Vector3 point, GameObject map) {
        Vector2 size = map.GetComponent<RectTransform>().rect.size;
        return new Vector2(Mathf.RoundToInt((point.x / WeatherManager.BoundsX) * (size.x)), Mathf.RoundToInt((point.z / WeatherManager.BoundsY) * (size.y)));
    }

    public void SetPlayer(GameObject player) {
        _playerUI = player.GetComponent<PlayerUI>();
        _playerBuildingManager = player.GetComponent<PlayerBuildingManager>();
        _playerGameObject = player;
    }
}
