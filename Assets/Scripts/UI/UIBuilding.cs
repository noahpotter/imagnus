﻿using UnityEngine;
using System.Collections;

public class UIBuilding : MonoBehaviour {

    [HideInInspector]
    public BuildingConfig building;

    private GameObject _player;
    private PlayerBuildingManager _playerBuildingManager;

    public void SetPlayer(GameObject player) {
        _player = player;
        _playerBuildingManager = _player.GetComponent<PlayerBuildingManager>();
    }

    public void StartBuilding() {
        _playerBuildingManager.StartBuilding(building);
    }
}
