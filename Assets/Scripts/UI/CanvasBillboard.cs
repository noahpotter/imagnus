﻿using UnityEngine;
using System.Collections;

public class CanvasBillboard : MonoBehaviour {

    public GameObject target;
	
	void Update () {
        transform.rotation = Quaternion.LookRotation(target.transform.forward);
	}
}
