﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
    Manages initializing the encyclopedia based on available resource data.
*/

public class UIEncyclopediaManager : MonoBehaviour {

    public GameObject resourcePrefab;
    public GameObject headerPrefab;
    public GameObject expanderPrefab;
    public GameObject constraintPrefab;

    private Transform _resourcesContainer;

	void Start () {
        _resourcesContainer = transform;
        foreach(ResourceTypes resourceType in System.Enum.GetValues(typeof(ResourceTypes))) {
            GameObject header = GameObject.Instantiate(headerPrefab);
            header.transform.Find("Text").GetComponent<Text>().text = resourceType.GetDescription();
            header.transform.SetParent(_resourcesContainer, false);

            foreach (Resource resource in Resource.GameResources) {
                if (resource.resourceType == resourceType) {
                    GameObject resourceEntry = Instantiate(resourcePrefab);
                    Transform expandedView = resourceEntry.transform.Find("Expanded View");
                    resourceEntry.transform.SetParent(_resourcesContainer, false);
                    resourceEntry.transform.Find("Collapsed View").Find("Title").Find("Text").GetComponent<Text>().text = resource.resource.GetDescription();
                    expandedView.Find("Title").Find("Text").GetComponent<Text>().text = resource.resource.GetDescription();

                    if (resource.useAirPressure) {
                        CreateConstraint(expandedView, "Air Pressure", resource.minAirPressure, resource.maxAirPressure);
                    }

                    if (resource.useElectromagnetism) {
                        CreateConstraint(expandedView, "Electromagnetism", resource.minElectromagnetism, resource.maxElectromagnetism);
                    }

                    if (resource.useHumidity) {
                        CreateConstraint(expandedView, "Humidity", resource.minHumidity, resource.maxHumidity);
                    }

                    if (resource.usePrecipitation) {
                        CreateConstraint(expandedView, "Precipitation", resource.minPrecipitation, resource.maxPrecipitation);
                    }

                    if (resource.useSunExposure) {
                        CreateConstraint(expandedView, "Sun Exposure", resource.minSunExposure, resource.maxSunExposure);
                    }

                    if (resource.useTemperature) {
                        CreateConstraint(expandedView, "Temperature", resource.minTemperature, resource.maxTemperature);
                    }

                    if (resource.useWindSpeed) {
                        CreateConstraint(expandedView, "Wind Speed", resource.minWindSpeed, resource.maxWindSpeed);
                    }

                    resourceEntry.GetComponent<UIResource>().Collapse();
                }
            }
        }

        GameObject expander = Instantiate(expanderPrefab);
        expander.transform.SetParent(_resourcesContainer, false);
	}

    private void CreateConstraint(Transform resourceEntry, string name, float min, float max) {
        GameObject constraint = Instantiate(constraintPrefab);
        constraint.transform.Find("Type").GetComponent<Text>().text = name;
        constraint.transform.Find("Range").GetComponent<Text>().text = ((int)min).ToString() + " - " + ((int)max).ToString();
        constraint.transform.SetParent(resourceEntry, false);
    }
	
	void Update () {
	
	}
}
