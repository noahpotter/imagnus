﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

/*
    Acts like a desktop window, allowing repositioning, resizing, and closing
*/

public class UIWindow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public Canvas canvas;
    private bool _isResizing;
    private bool _isMoving;
    private RectTransform _rectTransform;
    private Vector2 _startPosition;
    private Vector2 _startOffsetMax;
    private Vector2 _startOffsetMin;
    private Vector2 _startSizeDelta;
    private Vector2 _startMousePosition;

    public float minWidth, minHeight;

    public WindowManager windowManager;
    public Windows windowType;

	void Start () {
        _rectTransform = GetComponent<RectTransform>();
        ConstrainWindowPosition();
        ConstrainWindowSize();
	}
	
	void Update () {
	    if (_isResizing) {
            UpdateScale();
            ConstrainWindowSize();
        } else if (_isMoving) {
            UpdatePosition();
            ConstrainWindowPosition();
        }
    }

    public void UpdateScale() {
        Vector2 currentMousePosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform.parent as RectTransform, Input.mousePosition, canvas.worldCamera, out currentMousePosition);

        float width = _startSizeDelta.x + (currentMousePosition.x - _startMousePosition.x);
        float height = _startSizeDelta.y + (_startMousePosition.y - currentMousePosition.y);
        _rectTransform.sizeDelta = new Vector2(width, height);
        gameObject.BroadcastMessage("WindowResized", SendMessageOptions.DontRequireReceiver);
    }

    private void UpdatePosition() {
        Vector2 currentMousePosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform.parent as RectTransform, Input.mousePosition, canvas.worldCamera, out currentMousePosition);

        float x = _startPosition.x + (currentMousePosition.x - _startMousePosition.x);
        float y = _startPosition.y + (currentMousePosition.y - _startMousePosition.y);
        _rectTransform.localPosition = new Vector2(x, y);
    }

    public void StartResizing() {
        _isResizing = true;
        //_startOffsetMax = _rectTransform.offsetMax;
        //_startOffsetMin = _rectTransform.offsetMin;
        _startSizeDelta = _rectTransform.sizeDelta;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform.parent as RectTransform, Input.mousePosition, canvas.worldCamera, out _startMousePosition);
    }

    public void StopResizing() {
        _isResizing = false;
    }
    
    public void StartMoving() {
        _isMoving = true;
        _startPosition = _rectTransform.localPosition;
        transform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform.parent as RectTransform, Input.mousePosition, canvas.worldCamera, out _startMousePosition);
    }

    public void StopMoving() {
        _isMoving = false;
    }

    public void CloseWindow() {
        windowManager.HideWindow(windowType);
    }

    private void ConstrainWindowSize() {
        float maxWidth = (((RectTransform)canvas.transform).rect.width / 2) - _rectTransform.localPosition.x;
        float maxHeight = (((RectTransform)canvas.transform).rect.height / 2) + _rectTransform.localPosition.y;
        float currentWidth = Mathf.Clamp(_rectTransform.sizeDelta.x, minWidth, maxWidth);
        float currentHeight = Mathf.Clamp(_rectTransform.sizeDelta.y, minHeight, maxHeight);

        _rectTransform.sizeDelta = new Vector2(currentWidth, currentHeight);
    }

    private void ConstrainWindowPosition() {
        float canvasWidth = ((RectTransform)canvas.transform).rect.width;
        float canvasHeight = ((RectTransform)canvas.transform).rect.height;

        float maxX =  canvasWidth / 2 - _rectTransform.sizeDelta.x;
        float minY = -canvasHeight / 2 + _rectTransform.sizeDelta.y;

        float currentX = Mathf.Clamp(_rectTransform.localPosition.x, -canvasWidth/2, maxX);
        float currentY = Mathf.Clamp(_rectTransform.localPosition.y, minY, canvasHeight/2);

        _rectTransform.localPosition = new Vector2(currentX, currentY);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        FindObjectOfType<PlayerInput>().EnterWindow();
    }

    public void OnPointerExit(PointerEventData eventData) {
        FindObjectOfType<PlayerInput>().ExitWindow();
    }
}
