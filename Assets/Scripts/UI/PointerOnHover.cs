﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PointerOnHover : MonoBehaviour {

    public bool normalOnPointerUp;

    private EventTrigger _eventTrigger;

    void Awake() {
        _eventTrigger = GetComponent<EventTrigger>();

        if (_eventTrigger == null) {
            _eventTrigger = gameObject.AddComponent<EventTrigger>();
        }

        EventTrigger.Entry enterEntry = new EventTrigger.Entry();
        enterEntry.eventID = EventTriggerType.PointerEnter;
        enterEntry.callback.AddListener((eventData) => { PointerEnter(); });

        EventTrigger.Entry exitEntry = new EventTrigger.Entry();
        exitEntry.eventID = EventTriggerType.PointerExit;
        exitEntry.callback.AddListener((eventData) => { PointerExit(); });

        if (normalOnPointerUp) {
            EventTrigger.Entry upEntry = new EventTrigger.Entry();
            upEntry.eventID = EventTriggerType.PointerUp;
            upEntry.callback.AddListener((eventData) => { PointerExit(); });

            _eventTrigger.triggers.Add(upEntry);
        }

        _eventTrigger.triggers.Add(enterEntry);
        _eventTrigger.triggers.Add(exitEntry);
    }

    private void PointerEnter() {
        CursorManager.SetHover();
    }

    private void PointerExit() {
        CursorManager.SetNormal();
    }
}
