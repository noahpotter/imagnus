﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIPlayerInventory : MonoBehaviour {

    private PlayerInventory _playerInventory;
    private List<GameObject> _resources = new List<GameObject>();

	void Awake () {
	    foreach(Transform row in transform.FindChild("Inventory")) {
            foreach(Transform resource in row) {
                _resources.Add(resource.gameObject);
            }
        }
	}

    void Start() {
        _playerInventory = FindObjectOfType<PlayerInventory>();
    }

    void Update() {
        RefreshInventory();
    }
	
    public void RefreshInventory() {
        List<SpawnableResources> spawnableResources = System.Enum.GetValues(typeof(SpawnableResources)).Cast<SpawnableResources>().ToList();
        for(int i=0; i<_resources.Count; i++) {
            _resources[i].transform.Find("Number").GetComponent<Text>().text = _playerInventory.GetCount(spawnableResources[i]).ToString();
            _resources[i].transform.Find("Container").Find("Image").GetComponent<Image>().sprite = Resource.GetResource(spawnableResources[i]).sprite;
        }
    }
}
