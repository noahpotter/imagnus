﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HighlightOnHover : MonoBehaviour {

    public Image background;
    public Colors highlightColor;
    public Colors baseColor;

    public bool normalOnPointerUp;

    private EventTrigger _eventTrigger;

	void Awake () {
        _eventTrigger = GetComponent<EventTrigger>();

        if (_eventTrigger == null) {
            _eventTrigger = gameObject.AddComponent<EventTrigger>();
        }

        EventTrigger.Entry enterEntry = new EventTrigger.Entry();
        enterEntry.eventID = EventTriggerType.PointerEnter;
        enterEntry.callback.AddListener((eventData) => { PointerEnter(); });

        EventTrigger.Entry exitEntry = new EventTrigger.Entry();
        exitEntry.eventID = EventTriggerType.PointerExit;
        exitEntry.callback.AddListener((eventData) => { PointerExit(); });

        if (normalOnPointerUp) {
            EventTrigger.Entry upEntry = new EventTrigger.Entry();
            upEntry.eventID = EventTriggerType.PointerUp;
            upEntry.callback.AddListener((eventData) => { PointerExit(); });

            _eventTrigger.triggers.Add(upEntry);
        }

        _eventTrigger.triggers.Add(enterEntry);
        _eventTrigger.triggers.Add(exitEntry);
    }

    private void PointerEnter() {
        background.color = ColorManager.GetColor(highlightColor);
    }

    private void PointerExit() {
        background.color = ColorManager.GetColor(baseColor);
    }
}
