﻿using UnityEngine;
using System.Collections;

public class UIUpgradeBuilding : MonoBehaviour {

    [HideInInspector]
    public Building building;

    private GameObject _player;
    private PlayerBuildingManager _playerBuildingManager;

    public void SetPlayer(GameObject player) {
        _player = player;
        _playerBuildingManager = _player.GetComponent<PlayerBuildingManager>();
    }

    public void UpgradeBuilding() {
        _playerBuildingManager.UpgradeBuilding(building);
    }
}
