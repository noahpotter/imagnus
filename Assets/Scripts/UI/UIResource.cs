﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIResource : MonoBehaviour {

    private GameObject _collapsedView;
    private GameObject _expandedView;
    private Image _background;

	void Awake () {
        _collapsedView = transform.Find("Collapsed View").gameObject;
        _expandedView = transform.Find("Expanded View").gameObject;
        _background = transform.Find("Background").GetComponent<Image>();
    }

    public void Collapse() {
        _collapsedView.SetActive(true);
        _expandedView.SetActive(false);
        _background.color = ColorManager.SecondaryColor;
    }

    public void Expand() {
        _collapsedView.SetActive(false);
        _expandedView.SetActive(true);
        _background.color = ColorManager.SecondaryShadowColor;
    }

    public void ToggleCollapse() {
        if (_collapsedView.activeSelf) {
            Expand();
        } else {
            Collapse();
        }
    }
}
