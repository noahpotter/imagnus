﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBuildingsManager : MonoBehaviour {

    public static GameObject BuildingPrefab;
    public static GameObject UpgradePrefab;
    public static GameObject RequirementTypePrefab;
    public static GameObject RequirementPrefab;
    public static GameObject Player;

    public GameObject expanderPrefab;
    public GameObject buildingPrefab;
    public GameObject upgradePrefab;
    public GameObject requirementTypePrefab;
    public GameObject requirementPrefab;

    void Awake() {
        BuildingPrefab = buildingPrefab;
        UpgradePrefab = upgradePrefab;
        RequirementPrefab = requirementPrefab;
        RequirementTypePrefab = requirementTypePrefab;
    }

	void Start () {

        GameObject buildings = gameObject;

        foreach(BuildingConfig buildingConfig in BuildingConfig.BuildingConfigs) {
            GameObject buildingView = CreateBuildingView(buildingConfig);
            buildingView.transform.SetParent(buildings.transform, false);
        }

        GameObject expander = Instantiate(expanderPrefab);
        expander.transform.SetParent(transform, false);
	}

    public void SetPlayer(GameObject player) {
        Player = player;
    }

    public static GameObject CreateUpgradeView(Building building, int level) {
        GameObject upgradingView = Instantiate(UpgradePrefab);

        upgradingView.GetComponent<UIUpgradeBuilding>().building = building;
        upgradingView.GetComponent<UIUpgradeBuilding>().SetPlayer(Player);

        GameObject requirements = upgradingView.transform.Find("Info").Find("Requirement Types").gameObject;

        PopulateBuildingRequirementsView(requirements, building.buildingConfig, BuildingConfig.GetUpgradeRequirements(building.buildingConfig, level));

        return upgradingView;
    }

    public static GameObject CreateBuildingView(BuildingConfig buildingConfig) {
        GameObject buildingView = Instantiate(BuildingPrefab);

        buildingView.GetComponent<UIBuilding>().building = buildingConfig;
        buildingView.GetComponent<UIBuilding>().SetPlayer(Player);

        buildingView.transform.Find("Info").Find("Title").GetComponent<Text>().text = buildingConfig.type.GetDescription();
        GameObject requirements = buildingView.transform.Find("Info").Find("Requirement Types").gameObject;

        PopulateBuildingRequirementsView(requirements, buildingConfig, buildingConfig.requirements);

        return buildingView;
    }

    public static void PopulateBuildingRequirementsView(GameObject container, BuildingConfig buildingConfig, BuildingRequirement[] buildingRequirements) {
        foreach (ResourceTypes type in System.Enum.GetValues(typeof(ResourceTypes))) {
            GameObject typeView = Instantiate(RequirementTypePrefab);

            bool hasRequirementForType = false;

            foreach (BuildingRequirement requirement in buildingRequirements) {
                if (Resource.GetResource(requirement.resource).resourceType == type) {
                    hasRequirementForType = true;
                    GameObject requirementView = Instantiate(RequirementPrefab);
                    requirementView.transform.SetParent(typeView.transform, false);

                    requirementView.transform.Find("Number").GetComponent<Text>().text = requirement.quantity.ToString();
                    requirementView.transform.Find("Container").Find("Image").GetComponent<Image>().sprite = Resource.GetResource(requirement.resource).sprite;
                    //requirementView.transform.Find("Container").Find("Image").GetComponent<Text>().text = requirement.quantity.ToString();
                }
            }

            if (hasRequirementForType) {
                typeView.transform.SetParent(container.transform, false);
            } else {
                Destroy(typeView);
            }
        }
    }
}
