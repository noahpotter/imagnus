﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;

/*
    Initializes the map colors based on the value ranges.
    Manages player input on the map window. 
*/

public class UIMapManager : MonoBehaviour {

    public GameObject legend;
    public Slider timeSlider;
    public GameObject currentAttributeIndicatorPrefab;
    public int maxSliderSteps;

    [HideInInspector]
    public PlayerUI playerUI;

    // http://alert.fcd.maricopa.gov/alert/Google/image/Weather.png
    private Color[] _colors = new Color[] {
        new Color32(99, 99, 99, 255),
        new Color32(1, 232, 230, 255),
        new Color32(1, 156,240, 255),
        new Color32(2, 1, 241, 255),
        new Color32(2, 248, 2, 255),
        new Color32(0, 197, 1, 255),
        new Color32(2, 141, 2, 255),
        new Color32(249, 245, 1, 255),
        new Color32(228, 187, 0, 255),
        new Color32(248, 146, 2, 255),
        new Color32(248, 5, 2, 255),
        new Color32(208, 4, 2, 255),
        new Color32(182, 5, 3, 255),
        new Color32(245, 1, 248, 255),
        new Color32(151, 81, 197, 255),
        new Color32(254, 254, 254, 255)
    };

    public Color[] Colors { get { return _colors; } }

    private GameObject _legendSegmentPrefab;
    private MapManager _mapManager;
    private UITopographyManager _uiTopographyManager;
    private GameObject _attributesContainer;
    private Text _currentAttributeText;

    private int _currentSnapshot;
    public int CurrentSnapshot {
        get { return _currentSnapshot; }
        set {
            _currentSnapshot = value;
            _uiTopographyManager.ShowSnapshot();
        }
    }

    void Awake() {
        _attributesContainer = transform.Find("Attributes").gameObject;
        _attributesContainer.SetActive(false);
        _uiTopographyManager = GetComponent<UITopographyManager>();
        timeSlider.maxValue = maxSliderSteps;
        timeSlider.minValue = 1;
        CurrentSnapshot = 1;
        timeSlider.value = CurrentSnapshot;
        _currentAttributeText = transform.Find("Base").Find("Control Area").Find("Attribute").GetComponentInChildren<Text>();
    }

    void Start() {
        _legendSegmentPrefab = (GameObject) Resources.Load("Legend Segment");
        _mapManager = FindObjectOfType<MapManager>();

        int numRanges = 100 / _mapManager.segmentRange;
        for(int i=0; i<numRanges; i++) {
            GameObject legendSegment = Instantiate(_legendSegmentPrefab);

            legendSegment.transform.Find("Color Container").Find("Container").Find("Color").GetComponent<Image>().color = _colors[i];
            legendSegment.transform.Find("Text Container").Find("Text").GetComponent<Text>().text = (i*_mapManager.segmentRange).ToString() + "-" + ((i+1) * _mapManager.segmentRange - 1);

            legendSegment.transform.SetParent(legend.transform, false);
        }

        UpdateCurrentAttribute(playerUI.currentAttribute);
    }

    public void SetCurrentSnapshot() {
        CurrentSnapshot = Mathf.RoundToInt(timeSlider.value);
    }

    public void AddedSnapshot() {
        if (CurrentSnapshot > 1 && CurrentSnapshot < maxSliderSteps && CurrentSnapshot + 1 < _uiTopographyManager.SnapshotCount) {
            CurrentSnapshot++;
            timeSlider.value = CurrentSnapshot;
        }
    }

    public void SetTab(int i) {
        HideAttributes();
        UpdateCurrentAttribute((EnvironmentAttribute)i);
    }

    public void SetPlayer(GameObject player) {
        playerUI = player.GetComponent<PlayerUI>();
    }

    public void ShowAttributes() {
        _attributesContainer.SetActive(true);
    }

    public void HideAttributes() {
        _attributesContainer.SetActive(false);
    }

    private void UpdateCurrentAttribute(EnvironmentAttribute attr) {
        Transform currentAttributeIndicator = _attributesContainer.transform.Find(playerUI.currentAttribute.GetDescription()).Find("Current Attribute Indicator(Clone)");
        if (!currentAttributeIndicator) {
            currentAttributeIndicator = Instantiate(currentAttributeIndicatorPrefab).transform;
        }
        currentAttributeIndicator.transform.SetParent(_attributesContainer.transform.Find(attr.GetDescription()), false);
        playerUI.currentAttribute = attr;
        _currentAttributeText.text = attr.GetDescription();
    }
}
