﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum GroupSize {
    Small,
    Medium,
    Large
}

/*
    Configures how groups can be spawned
*/

public class GroupConfig : MonoBehaviour {
    [Space]
    public float spawnChance;

    [Header("Group")]
    public GroupSize groupSize;
    [Range(0, 5)]
    public int minGroupCount;
    [Range(0, 5)]
    public int maxGroupCount;

    public float minGroupSpeed;
    public float maxGroupSpeed;
    public float minPercentNest;
    public float maxPercentNest;

    [Header("Node")]
    [Range(0, 20)]
    public int minNodeCount;
    [Range(0, 20)]
    public int maxNodeCount;
    [Range(0, 5)]
    public int minNodeConcentration;
    [Range(0, 5)]
    public int maxNodeConcentration;
    [Space]
    public float minNodeDistance;
    public float maxNodeDistance;
    public float minNodeStrength;
    public float maxNodeStrength;
    public float minNodeDecay;
    public float maxNodeDecay;
}
