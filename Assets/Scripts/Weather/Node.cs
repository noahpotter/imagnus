﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
    A Node is a basic unit used to describe the current weather. It consists of a strength, how that 
    strength acts over distance, and how fast it decays.
    It also has a position within its parent group.
    Nodes must have a parent group
*/

public class Node {
    public Vector2 Position { get; set; }
    public Vector2 WorldPosition {
        get {
            if (parentGroup != null) {
                return parentGroup.WorldPosition + Position;
            } else {
                return Position;
            }
        }
    }

    public Group parentGroup;

    public float strength;
    public float strengthVariance = 0.05f; // Strength mod

    public float maxDistance;
    public int concentration;

    public float decay;
    public float decayVariance = 0.05f; // Decay mod

    /* STATIC */

    public static List<Node> GetAllNodes(Group parentGroup) {
        if (parentGroup.Groups.Count == 0) {
            return parentGroup.Nodes;
        } else {
            List<Node> nodes = new List<Node>();
            nodes.AddRange(parentGroup.Nodes);
            foreach (Group group in parentGroup.Groups) {
                nodes.AddRange(GetAllNodes(group));
            }
            return nodes;
        }
    }

    public static List<Node> GetAllNodes(List<Group> groups) {
        List<Node> nodes = new List<Node>();
        foreach(Group group in groups) {
            nodes.AddRange(GetAllNodes(group));
        }
        return nodes;
    }
}
