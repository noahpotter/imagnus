﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

/*
    A Group is a grouping of nodes in a specific configuration. Groups can contain more groups
    Generates groups.
    Holds child nodes, groups, and parent groups
*/

public class Group {
    private List<Node> _nodes= new List<Node>();
    public List<Node> Nodes {
        get {
            foreach(Node node in _nodes) {
                if (node.parentGroup == null) {
                    node.parentGroup = this;
                }
            }
            return _nodes;
        }
        set {
            _nodes = value;
        }
    }
    private List<Group> _groups = new List<Group>();
    public List<Group> Groups {
        get {
            foreach (Group group in _groups) {
                if (group.parentGroup == null) {
                    group.parentGroup = this;
                }
            }
            return _groups;
        }
        set {
            _groups = value;
        }
    }

    private Vector2 _position = Vector2.zero;
    public Vector2 Position {
        get {
            return _position;
        }
        set {
            _position = value;
        }
    }
    public Vector2 WorldPosition {
        get {
            if (parentGroup == null) {
                return Position;
            } else {
                return parentGroup.WorldPosition + Position;
            }
        }
    }

    public Group parentGroup;

    private Vector2 _direction;
    public Vector2 Direction {
        get {
            if (parentGroup == null) {
                return _direction;
            } else {
                return parentGroup.Direction;
            }
        }
        set {
            if (parentGroup == null) {
                _direction = value;
            } else {
                parentGroup.Direction = value;
            }
        }
    }

    private float _directionVariance;
    public float DirectionVariance {
        get {
            if (parentGroup == null) {
                return _directionVariance;
            } else {
                return parentGroup.DirectionVariance;
            }
        }
        set {
            if (parentGroup == null) {
                _directionVariance = value;
            } else {
                parentGroup.DirectionVariance = value;
            }
        }
    }

    private float _speed;
    public float Speed {
        get {
            if (parentGroup == null) {
                return _speed;
            } else {
                return parentGroup.Speed;
            }
        }
        set {
            if (parentGroup == null) {
                _speed = value;
            } else {
                parentGroup.Speed = value;
            }
        }
    }

    private float _speedVariance;
    public float SpeedVariance {
        get {
            if (parentGroup == null) {
                return _speedVariance;
            } else {
                return parentGroup.SpeedVariance;
            }
        }
        set {
            if (parentGroup == null) {
                _speedVariance = value;
            } else {
                parentGroup.SpeedVariance = value;
            }
        }
    }


    public void AddNode(Node node) {
        node.parentGroup = this;
        Nodes.Add(node);
    }

    public void RemoveNode(Node node) {
        node.parentGroup = null;
        Nodes.Remove(node);
    }

    public void AddGroup(Group group) {
        group.parentGroup = this;
        Groups.Add(group);
    }

    public void RemoveGroup(Group group) {
        group.parentGroup = null;
        Groups.Remove(group);
    }
    public void RemoveFromParentGroup() {
        parentGroup.RemoveGroup(this);
        parentGroup = null;
    }

    /* STATIC */

    // Given a group, return a collection of parentGroup and all children flattened recursively
    public static List<Group> GetAllGroups(Group parentGroup) {
        List<Group> groups = new List<Group>() { parentGroup };

        foreach (Group group in parentGroup.Groups) {
            groups.AddRange(GetAllGroups(group));
        }
        return groups;
    }

    // Given a list of groups, return a collection of flattened given groups and their children
    public static List<Group> GetAllGroups(List<Group> groups) {
        List<Group> allGroups = new List<Group>();

        foreach (Group group in groups) {
            allGroups.AddRange(GetAllGroups(group));
        }
        return allGroups;
    }

    public static void CleanUpOrphans(List<Group> groups) {
        foreach(Group group in groups.ToList()) {
            if (group.Nodes.Count == 0 && group.Groups.Count == 0) {
                //Debug.Log("Cleaned up orphan");
                if (group.parentGroup != null) {
                    //Debug.Log("Removing from parent");
                    group.parentGroup.RemoveGroup(group);
                } else {
                    //Debug.Log("Removing from groups");
                    groups.Remove(group);
                }
            }
        }
    }

    public static List<Group> GenerateGroups() {
        List<Group> groups = new List<Group>();

        return groups;
    }

    public static Group GenerateGroup(GroupConfig groupConfig) {
        return GenerateGroup(
            UnityEngine.Random.Range(groupConfig.minGroupCount, groupConfig.maxGroupCount),
            () => (UnityEngine.Random.Range(groupConfig.minNodeCount, groupConfig.maxGroupCount)),
            () => (UnityEngine.Random.Range(groupConfig.minPercentNest, groupConfig.maxPercentNest)),
            () => (UnityEngine.Random.Range(groupConfig.minNodeStrength, groupConfig.maxNodeStrength)),
            () => (UnityEngine.Random.Range(groupConfig.minNodeDistance, groupConfig.maxNodeDistance)),
            () => (UnityEngine.Random.Range(groupConfig.minNodeDecay, groupConfig.maxNodeDecay)),
            () => (UnityEngine.Random.Range(groupConfig.minGroupSpeed, groupConfig.maxGroupSpeed)),
            () => (UnityEngine.Random.Range(groupConfig.minNodeConcentration, groupConfig.maxNodeConcentration))
        );
    }

    // Should randomly chose a direction or two and have a certain percent of favoring that direction when adding nodes and groups
    public static Group GenerateGroup(int numGroups, 
                                      Func<int> GetNumNodes, 
                                      Func<float> GetPercentNest, 
                                      Func<float> GetNodeStrength, 
                                      Func<float> GetNodeMaxDistance, 
                                      Func<float> GetNodeDecay, 
                                      Func<float> GetGroupSpeed, 
                                      Func<int> GetNodeConcentration,
                                      Group parentGroup = null, int currentNumGroups = 0) {
        Group group = new Group();
        group.Speed = GetGroupSpeed();
        group.SpeedVariance = 0.1f;
        List<Node> blockingNodes = new List<Node>();
        currentNumGroups++;

        if (parentGroup != null) {
            blockingNodes = Node.GetAllNodes(parentGroup);
        }

        int[] weights = new int[] { 1, 1, 1, 1 };
        weights[UnityEngine.Random.Range(0, 4)] = 4;

        // First add nodes
        for (int i = 0; i < GetNumNodes(); i++) {
            Node node = new Node();
            group.AddNode(node);
            Vector2? newPosition;

            if (i == 0) { // If this is the first time, loop through all nodes trying to find a valid starting point
                newPosition = GetAvailableNeighborPosition(new List<Node>(), blockingNodes, weights).GetValueOrDefault();
            } else { // We found a node last iteration so just build from that
                newPosition = GetAvailableNeighborPosition(blockingNodes, group.Nodes, weights).GetValueOrDefault();
            }

            if (newPosition != null) {
                node.Position = (Vector2)newPosition;
                node.strength = GetNodeStrength();
                node.maxDistance = GetNodeMaxDistance();
                node.decay = GetNodeDecay();
                node.concentration = GetNodeConcentration();
                blockingNodes.Add(node);
            } else {
                // We won't find any more valid position, assume we're done
                group.RemoveNode(node);
                Debug.Log("Couldn't find any valid positions");
                break;
            }
        }

        // Then add more groups if needed
        for (int i=currentNumGroups; i<numGroups; /* add to i based on the number of groups created */) {
            Group nextGroup;

            // We have to add another nested group
            if (parentGroup == null || UnityEngine.Random.Range(0, 1) < GetPercentNest()) {
                // We should nest 
                nextGroup = GenerateGroup(numGroups, GetNumNodes, GetPercentNest, GetNodeStrength, GetNodeMaxDistance, GetNodeDecay, GetGroupSpeed, GetNodeConcentration, group, currentNumGroups);
                group.AddGroup(nextGroup);
                i += GetAllGroups(nextGroup).Count;
            } else {
                // Let the parent handle adding more groups
                break;
            }
        }

        return group;
    }

    // Can select a position beside a validNode, can't select positions that invalid nodes occupy
    public static Vector2? GetAvailableNeighborPosition(List<Node> blockingNodes, List<Node> validNodes, int[] weights) {
        if (validNodes.Count == 0) {
            return Vector2.zero;
        }

        validNodes.Shuffle();
        List<Vector2> directions = new List<Vector2> {
            new Vector2(1, 1),
            new Vector2(-1, 1),
            new Vector2(1, -1),
            new Vector2(-1, -1)
        };

        foreach (Node node in validNodes) {
            List<float> calculatedWeights = new List<float>();
            for(int i=0; i<4; i++) {
                calculatedWeights.Add(UnityEngine.Random.Range(0f, 1f) * weights[i]);
            }
            directions = directions.Select((vector, index) => new KeyValuePair<int, Vector2>(index, vector)).OrderByDescending(pair => calculatedWeights[pair.Key]).Select(x => x.Value).ToList();

            foreach (Vector2 direction in directions) {
                Vector2 newPosition = node.WorldPosition + direction;
                if (IsOpen(newPosition, validNodes) && IsOpen(newPosition, blockingNodes)) {
                    return newPosition;
                }
            }
        }

        // There weren't any valid locations
        return null;
    }

    public static bool IsOpen(Vector2 position, List<Node> nodes) {
        foreach(Node node in nodes) {
            if (position.ToIntVector2().Equals(node.WorldPosition.ToIntVector2())) {
                return false;
            }
        }
        return true;
    }
}
