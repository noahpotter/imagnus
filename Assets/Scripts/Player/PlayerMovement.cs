﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
    Manages moving the player and playing animations
*/

public class PlayerMovement : MonoBehaviour {

    public float speed = 2.0F;
    public Animator animator;
    public GameObject model;

    private PlayerCamera _playerCamera;

    private bool _canMove = true;
    public bool canMove {
        get {
            return _canMove;
        }
        set {
            if (!value && isMoving) {
                isMoving = false;
            }

            _canMove = value;
        }
    }

    private bool _isMoving = false;
    public bool isMoving {
        get { return _isMoving; }
        set {
            if (_isMoving && !value) {
                animator.SetTrigger("Waiting");
            } else if (!_isMoving && value) {
                animator.SetTrigger("Walking");
            }
            _isMoving = value;
        }
    }

    private CharacterController _characterController;
    private Vector3 moveDirection = Vector3.zero;

    void Start () {
        _playerCamera = GetComponent<PlayerCamera>();
        _characterController = GetComponent<CharacterController>();
	}
	
	void Update () {
        moveDirection = Vector3.zero;
        if (_characterController.isGrounded && canMove) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            float magnitude = moveDirection.magnitude;
            moveDirection = _playerCamera.spawnedCamera.transform.TransformDirection(moveDirection);
            moveDirection = Vector3.ProjectOnPlane(moveDirection, Vector3.up);
            moveDirection = moveDirection.normalized * magnitude;
            moveDirection *= speed;

            if (moveDirection.magnitude > 0.1f) {
                isMoving = true;
                model.transform.rotation = Quaternion.LookRotation(moveDirection);
            } else {
                isMoving = false;
            }
        }

        _characterController.SimpleMove(moveDirection);
    }
}
