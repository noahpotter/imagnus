﻿using UnityEngine;
using System.Collections;

/*
    Manages input given by the player.
    Handles when the player leaves the screen.
*/

public class PlayerInput : MonoBehaviour {

    private PlayerCamera _playerCamera;
    private PlayerUI _playerUI;
    private PlayerBuildingManager _playerBuildingManager;
    private PlayerMovement _playerMovement;
    private PlayerInventory _playerInventory;

    private bool _canZoom = true;
    private float _rightButtonDownTime;
    private float _cancelBuildTime = 0.2f;

    void Awake() {
        _playerCamera = GetComponent<PlayerCamera>();
        _playerUI = GetComponent<PlayerUI>();
        _playerBuildingManager = GetComponent<PlayerBuildingManager>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerInventory = GetComponent<PlayerInventory>();
    }

    void Update() {
        Rect screenRect = new Rect(1, 1, Screen.width - 1, Screen.height - 1);

        if (screenRect.Contains(Input.mousePosition)) {
            _playerCamera.spawnedCamera.isZoomingCamera = _canZoom;
            _playerMovement.canMove = true;

            if (Input.GetMouseButtonDown(1)) {
                _playerCamera.SetIsMovingCamera(true);
            }

            if (Input.GetButtonDown("Toggle Map")) {
                _playerUI.ToggleMap();
            }

            if (Input.GetButtonDown("Toggle Encyclopedia")) {
                _playerUI.ToggleEncyclopedia();
            }

            if (Input.GetButtonDown("Toggle Buildings")) {
                _playerUI.ToggleBuildings();
            }

            if (_playerBuildingManager.IsBuilding) {
                Ray ray = _playerCamera.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100000, LayerMask.GetMask("Floor"))) {
                    _playerBuildingManager.SetCurrentPosition(hit);
                }

                if (Input.GetMouseButtonDown(0)) {
                    _playerBuildingManager.PlayerCreateBuilding();
                } else if (Input.GetMouseButtonDown(1)) {
                    _rightButtonDownTime = Time.time;
                } else if (Input.GetMouseButtonUp(1)) {
                    if (Time.time - _rightButtonDownTime < _cancelBuildTime) {
                        _playerBuildingManager.CancelBuilding();
                    }
                }
            }

            if (Input.GetButtonDown("Toggle Sensors")) {
                FindObjectOfType<UITopographyManager>().useSensors = !FindObjectOfType<UITopographyManager>().useSensors;
            } else if (Input.GetButtonDown("Decrease Speed")) {
                GameTimeManager.DecreaseSpeed();
            } else if (Input.GetButtonDown("Increase Speed")) {
                GameTimeManager.IncreaseSpeed();
            } else if (Input.GetButtonDown("Fill Inventory")) {
                _playerInventory.AddToAll(20);
            }
        } else {
            _playerCamera.spawnedCamera.isZoomingCamera = false;
            _playerMovement.canMove = false;
        }

        if (Input.GetMouseButtonUp(1)) {
            _playerCamera.SetIsMovingCamera(false);
        }
    }

    public void EnterWindow() {
        _canZoom = false;
    }

    public void ExitWindow() {
        _canZoom = true;
    }
}
