﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlayerCamera : MonoBehaviour {

    public GameObject cameraPrefab;
    [HideInInspector]
    public SpawnedCamera spawnedCamera;
    [HideInInspector]
    public new Camera camera;
    [HideInInspector]
    public Camera uiCamera;
    [HideInInspector]
    public Camera buildingUICamera;

    void Awake() {
        spawnedCamera = Instantiate(cameraPrefab).GetComponent<SpawnedCamera>();
        spawnedCamera.target = transform;
        camera = spawnedCamera.GetComponent<Camera>();
        uiCamera = spawnedCamera.transform.Find("UI Camera").GetComponent<Camera>();
        buildingUICamera = spawnedCamera.transform.Find("Building UI Camera").GetComponent<Camera>();
        spawnedCamera.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>().focalTransform = transform;
    }

    public void SetIsMovingCamera(bool f) {
        spawnedCamera.isMovingCamera = f;
    }
}
