﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
    Talks to the UI that corresponds to this player
*/

public class PlayerUI : MonoBehaviour {
    
    public EnvironmentAttribute currentAttribute = 0;
    public GameObject canvasPrefab;

    private WindowManager _windowManager;
    private Canvas _canvas;
    private PlayerCamera _playerCamera;

    void Awake() {
        _canvas = Instantiate(canvasPrefab).GetComponent<Canvas>();
        _playerCamera = GetComponent<PlayerCamera>();
        _windowManager = _canvas.GetComponent<WindowManager>();
        _windowManager.player = gameObject;

        _canvas.renderMode = RenderMode.ScreenSpaceCamera;
        _canvas.planeDistance = 0.1f;
    }

    void Start() {
        _canvas.worldCamera = _playerCamera.spawnedCamera.transform.Find("UI Camera").GetComponent<Camera>();
    }

    public void HideWindows() {
        _windowManager.HideWindows();
    }

    public void ShowWindows() {
        _windowManager.ShowWindows();
    }

    public void ToggleMap() {
        _windowManager.ToggleWindow(Windows.Map);
    }

    public void ToggleEncyclopedia() {
        _windowManager.ToggleWindow(Windows.Encyclopedia);
    }

    public void ToggleBuildings() {
        _windowManager.ToggleWindow(Windows.Buildings);
    }
}
