﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/*
    Keeps track of the current items the player has.
*/

public class PlayerInventory : MonoBehaviour {

    public int startQuantities;
    public Dictionary<SpawnableResources, int> inventory = new Dictionary<SpawnableResources, int>();

	void Awake () {
        foreach (SpawnableResources spawnableResource in System.Enum.GetValues(typeof(SpawnableResources))) {
            inventory.Add(spawnableResource, 0);
        }

        AddToAll(startQuantities);
	}

    public bool HasEnough(SpawnableResources spawnableResource, int count) {
        return inventory[spawnableResource] >= count;
    }

    public void RemoveFromInventory(SpawnableResources spawnableResource, int count) {
        inventory[spawnableResource] -= count;
        if (inventory[spawnableResource] < 0) {
            inventory[spawnableResource] = 0;
        }
    }

    public void AddToInventory(SpawnableResources spawnableResource, int count) {
        inventory[spawnableResource] += count;
    }

    public int GetCount(SpawnableResources spawnableResource) {
        return inventory[spawnableResource];
    }

    public void UseResources(List<BuildingRequirement> requirements) {
        foreach (BuildingRequirement requirement in requirements) {
            RemoveFromInventory(requirement.resource, requirement.quantity);
        }
    }

    public void AddToAll(int quantity) {
        foreach (SpawnableResources spawnableResource in System.Enum.GetValues(typeof(SpawnableResources))) {
            inventory[spawnableResource] += quantity;
        }
    }
}
