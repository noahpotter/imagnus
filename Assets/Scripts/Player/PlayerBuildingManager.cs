﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
    Manages building buildings for the player. Makes sure the player has enough resources to build and is targeting a valid position.
*/

public class PlayerBuildingManager : MonoBehaviour {

    public bool IsBuilding { get { return _currentBuilding != null; } }

    private List<Building> _buildings = new List<Building>();

    private Vector3 _currentBuildPosition;
    private Quaternion _currentBuildingRotation;

    private PlayerUI _playerUI;
    private PlayerInventory _playerInventory;

    private BuildingConfig _currentBuilding;
    private GameObject _currentBuildingPlacement;
    private bool _foundValidSpot;

	void Awake () {
        _playerUI = GetComponent<PlayerUI>();
        _playerInventory = GetComponent<PlayerInventory>();

        _buildings = FindObjectsOfType<Building>().ToList();
	}

    public void StartBuilding(BuildingConfig building) {
        if (BuildingConfig.PlayerCanBuild(building, _playerInventory)) {
            _playerUI.HideWindows();
            _currentBuilding = building;
            _currentBuildingPlacement = BuildingConfig.CreatePlacementModel(building);
            _foundValidSpot = false;
        }
    }

    public void PlayerCreateBuilding() {
        if (_foundValidSpot) {
            _playerUI.ShowWindows();
            Destroy(_currentBuildingPlacement);

            Building building = BuildingConfig.Build(_currentBuilding, _currentBuildPosition, _currentBuildingRotation, gameObject);
            _playerInventory.UseResources(building.buildingConfig.requirements.ToList());
            AddBuilding(building);

            _currentBuilding = null;
        }
    }

    public void AddBuilding(Building building) {
        _buildings.Add(building);
    }

    public void CancelBuilding() {
        _currentBuilding = null;
        _playerUI.ShowWindows();
        Destroy(_currentBuildingPlacement);
    }

    public void UpgradeBuilding(Building building) {
        if (BuildingConfig.PlayerCanUpgrade(building, _playerInventory)) {
            _playerInventory.UseResources(BuildingConfig.GetUpgradeRequirements(building));
            building.Upgrade();
        }
    }

    public void SetCurrentPosition(RaycastHit hit) {
        Vector3 targetPoint = hit.point;
        Vector3 pointAbove = targetPoint;
        pointAbove.y = 100;
        Vector3 playerPositionAbove = transform.position;
        playerPositionAbove.y = 100;

        // Bring point into range if too far
        if (Vector3.Distance(pointAbove, playerPositionAbove) > BuildingConfig.BuildRadius) {
            pointAbove = playerPositionAbove + ((pointAbove - playerPositionAbove).normalized * (BuildingConfig.BuildRadius - 0.1f));
        }

        RaycastHit finalHit;
        bool hitSomething = Physics.Raycast(pointAbove, Vector3.down, out finalHit, 200, LayerMask.GetMask("Floor"), QueryTriggerInteraction.Collide);

        //Debug.Log(finalHit.collider.name);
        if (hitSomething && BuildingConfig.CanBuildHere(_currentBuilding, finalHit.point)) {
            _foundValidSpot = true;
            _currentBuildPosition = finalHit.point;
            Vector3 currentPosition = transform.position;
            currentPosition.y = 0;
            Vector3 currentPoint = finalHit.point;
            currentPoint.y = 0;
            _currentBuildingRotation = Quaternion.LookRotation(currentPosition - currentPoint, Vector3.up);
            UpdateBuildingPlacement();
        }
    }

    private void UpdateBuildingPlacement() {
        _currentBuildingPlacement.transform.position = _currentBuildPosition;
        _currentBuildingPlacement.transform.rotation = _currentBuildingRotation;
    }

    public List<Building> GetSensors() {
        return _buildings.Where(x => x.buildingConfig.type == BuildingTypes.Sensor).ToList();
    }
}
